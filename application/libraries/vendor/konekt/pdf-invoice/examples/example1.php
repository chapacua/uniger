<?php
include('../src/InvoicePrinter.php');
$invoice = new InvoicePrinter();
  /* Header Settings */
  $invoice->setTimeZone('America/Bogota');
  $invoice->setLogo("images/postobon.png");
  $invoice->setColor("#DD1A12");
  $invoice->setType("Factura de Venta");
  $invoice->setReference("PRUE-95000001");
  $invoice->setDate(date('M dS ,Y',time()));
  $invoice->setTime(date('h:i:s A',time()));
  $invoice->setDue(date('M dS ,Y',strtotime('+3 months')));
  $invoice->setFrom(array("Postobon S.A.S","Sample Company Name","128 AA Juanita Ave","Glendora , CA 91740","United States of America"));
  $invoice->setTo(array("Tienda Beto","Sample Company Name","128 AA Juanita Ave","Glendora , CA 91740","United States of America"));
  /* Adding Items in table */
  $invoice->addItem("Caja Gaseosa Manzana","Manzana postobon 30 unidades",6,0,580,0,3480);
  /* Add totals */
  $invoice->addTotal("Total",9460);
  $invoice->addTotal("IVA 19%",1986.6);
  $invoice->addTotal("Total a pagar",11446.6,true);
  /* Set badge */ 
  $invoice->addBadge("Por pagar");
  /* Add title */
  $invoice->addTitle("Nota importante");
  /* Add Paragraph */
  $invoice->addParagraph("Ningún artículo será reemplazado o reembolsado si no tiene la factura con usted. Puede reembolsar dentro de 2 días de compra.");
  /* Set footer note */
  $invoice->setFooternote("Postobon");
  /* Render */
  $invoice->render('example1.pdf','I'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
?>
