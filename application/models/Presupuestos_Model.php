<?php
class Presupuestos_Model extends CI_Model

{

    public function traer_detalles_pesupuestos()
    {
        $this->db->select('d.*, i.Inm_Tipo, p.Per_Periodo');
        $this->db->join('Inmuebles i', 'd.Inm_Id = i.Inm_Id', 'left');
        $this->db->join('Presupuestos p', 'd.Pre_Id = p.Pre_Id', 'left');
        $this->db->group_by('Per_Periodo, Inm_Id');
        $query = $this->db->get('Detalle_Presupuesto d');

        return $query->result();
    }
    
    public function traer_presupuestos()
    {
        $this->db->select('*');
        $query = $this->db->get('Presupuestos');
        
        return $query->result();
    }
    
    public function borrar_presupuesto($id)
    {
        $this->db->select('Per_Periodo');
        $query = $this->db->get('Presupuestos');
        
        
        $this->db->where('Pre_Id', $id);
        $this->db->delete('Presupuestos');
        
        return $query->result()[0]->Per_Periodo;
    }

    public function borrar_detalles_presupuesto($periodo)
    {
        $this->db->where('Pre_Periodo', $periodo);
        $this->db->delete('Detalle_Presupuesto');
    }

	/****************/
	/*              */
	/* Configuracion*/
	/*              */
	/****************/

    /**
	 * Crea un nuevo periodo de facturaciÃ³n para recibos de pago
	 *
	 * @param       N/A
	 * @return      N/A
	 */
    public function conf_nuevo_periodo()
    {

		// Establece el periodo para el aÃ±o en curso y deshabilita el del aÃ±o anterior
		// Query 1

		$this->db->set('Per_Estado', 0);
		$this->db->where('Per_Periodo', 'YEAR(CURDATE())', false);
		$this->db->update('periodo');

		// Se ingresa el periodo consultado + 1
		// Query 2
		// Se consulta el Ãºltimo periodo registrado

		$this->db->select_max('Per_Periodo');
		$maxPeriodo = $this->db->get('periodo');
		$maxPeriodo = $maxPeriodo->result() [0]->Per_Periodo;

		// Se inserta el nuevo periodo
		// Query 3

		$this->db->set('Per_Periodo', ($maxPeriodo + 1));
		$this->db->insert('periodo');

		// Se cambia el estado a activo para el nuevo periodo
		// Query 4

		$this->db->set('Per_Estado', 1);
		$this->db->where('Per_Periodo', 'YEAR(CURDATE())+1', false);
		$this->db->update('periodo');
    }

    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function conf_traer_periodos_sin_presupuesto()
    {

        $this->db->select('p.Per_Periodo', false);
        $this->db->from('Periodo as p');
        $this->db->join('Presupuestos AS pre', 'p.`Per_Periodo` = pre.`Per_Periodo`', 'left');
        $this->db->where('pre.`Per_Periodo` IS NULL', '', false);        
        $query = $this->db->get();

        return $query->result();
    }

    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function conf_traer_meses_disponibles()
    {

        $this->db->select('*');    
        $this->db->where('Mes_Id >', 'MONTH(CURDATE())', false);
        
        $query = $this->db->get('Mes');

        return $query->result();
    }

    /**
	 * Guarda el nuevo presupuesto general para un periodo en especifico
	 *
	 * @param       array  $datos
	 * @return      int $insert_id
	 */   
	function conf_nuevo_presupuesto($datos)
    {
    
        $this->db->insert('Presupuestos', $datos);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    /**
	 * Crea los detalles del cobro mes a mes ques erá realizado por inmueble
	 *
	 * @param       array  $datos
	 * @return      N/A
	 */
	function conf_nuevo_detalle_presupuesto($valor, $id_presupuesto, $descripcion)
    {
        $inmuebles = $this->inm_traer_datos_presupuesto();
        foreach ($inmuebles as $inmueble) {
            $total_por_periodo_inm = round($valor*$inmueble->Inm_Coeficiente_Propiedad);
            for ($i=1; $i <= 12; $i++) { 

                $total_por_mes_inm = round($total_por_periodo_inm/12);
                
                $datos = array (
                    'Pre_Id' => $id_presupuesto,
                    'Inm_Id' => $inmueble->Inm_Id,
                    'Mes_Id' => $i,
                    'Det_Pre_Valor' => $total_por_mes_inm,
                    'Det_Pre_Descripcion' => $descripcion
                );

                $this->db->insert('Detalle_Presupuesto', $datos);

            }
        }
        
    }
    
    /**
     * Crea los detalles del cobro mes a mes ques erá realizado por inmueble
	 *
     * @param       N/A
     * @return      array/object  $query->result()
	 */
    function inm_traer_datos_presupuesto()
    {
        
        $this->db->select('Inm_Id, Inm_Coeficiente_Propiedad');
        $query = $this->db->get('Inmuebles');
        return $query->result();
    }
    
    /**
     * Guarda el nuevo valor para ser recaudadopor concepto de cuota extraordinaria
     *
     * @param       array  $datos
     * @return      int $insert_id
     */   
    function conf_nueva_cuota_extraordinaria($datos)
    {
    
        $this->db->insert('Cuota_Extraordinaria', $datos);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    /**
     * Consulta los periodos mayores o iguales al periodo actual
     *
     * @param       array  $datos
     * @return      int $insert_id
     */   
    function conf_traer_periodos()
    {
    
        $this->db->select('Per_Periodo');
        $this->db->where('Per_Periodo >=', 'YEAR(CURDATE())', false);
        $query = $this->db->get('Periodo');

        return $query->result();
    }

    /**
     * Crea los detalles del cobro de cuota extraordinaria ques erá realizado por inmueble
     *
     * @param       array  $datos
     * @return      N/A
     */
    function conf_nuevo_detalle_cuota_extraordinaria($valor, $id_presupuesto, $descripcion)
    {
        $inmuebles = $this->inm_traer_datos_presupuesto();
        foreach ($inmuebles as $inmueble) {
            for ($i=1; $i <= 12; $i++) { 

                $total_por_mes_inm = round($valor*$inmueble->Inm_Coeficiente_Propiedad);
                
                $datos = array (
                    'Cuo_Ext_Id' => $id_presupuesto,
                    'Inm_Id' => $inmueble->Inm_Id,
                    'Mes_Id' => $i,
                    'Det_Cuo_Ext_Valor' => $total_por_mes_inm,
                    'Det_Cuo_Ext_Descripcion' => $descripcion
                );

                $this->db->insert('Detalle_Cuota_Extraordinaria', $datos);

            }
        }
        
    }

    /**
     *Valida si un campo tiene el formato de la fecha correctamente
     *
     * @param       date  $date
     * @return      true/false
    */
    function crear_recibos_pago($mes_actual, $inmuebleProcesar, $periodo_actual, $recibo_id){

        $this->db->select('d.Det_Pre_Id');    
        $this->db->where('d.Mes_Id', $mes_actual);
        $this->db->where('d.Inm_Id', $inmuebleProcesar);
        $this->db->join('Presupuestos pre', 'd.Pre_Id  = pre.Pre_Id');
        $this->db->join('Periodo p', 'p.Per_Periodo  = pre.Per_Periodo');
        $this->db->where('p.Per_Periodo', $periodo_actual);
        $query = $this->db->get('Detalle_Presupuesto d');
        foreach($query->result() as $row)
        {
            $datos = array(
                'Det_Pre_Id' => $row->Det_Pre_Id,
                'Det_Rec_Pag_Estado' => 'Por Imprimir',
                'Rec_Pag_Id' => $recibo_id
            );

            $this->db->insert('Detalle_Recibo_Pago', $datos);
            
        }
    }

    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function traer_presupuestos_por_recibo($Rec_Pag_Id)
    {

        $this->db->select('Rec_Pag_Id');
        $this->db->where('Rec_Pag_Pdf = ', $Rec_Pag_Id.".pdf");
        $query = $this->db->get('Recibo_Pago');
        
        $this->db->select('dpre.`Det_Pre_Valor`, DATEDIFF(CURDATE(), ADDDATE(rp.`Reg_Pag_Fecha`, INTERVAL 10 DAY)) AS Rec_Pag_Dias_Vencidos');
        $this->db->join('`Detalle_Presupuesto` AS dpre', 'd.`Det_Pre_Id` = dpre.`Det_Pre_Id`');
        $this->db->join('Recibo_Pago AS rp', 'd.`Rec_Pag_Id` = rp.`Rec_Pag_Id`');
        $this->db->where('d.`Rec_Pag_Id` =', $query->result()[0]->Rec_Pag_Id);
        
        $query = $this->db->get('Detalle_Recibo_Pago AS d');
        return $query->result();
    }

        /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function traer_id_recibo_pago($Rec_Pag_Id)
    {

        $this->db->select('Rec_Pag_Id');
        $this->db->where('Rec_Pag_Pdf = ', $Rec_Pag_Id.".pdf");
        $query = $this->db->get('Recibo_Pago');

        return $query->result()[0]->Rec_Pag_Id;
    }
    public function reportes_anual()
    {
        $this->db->select('Pre_valor, Per_periodo');
        $this->db->from('Presupuestos');
        $query = $this->db->get();
        return $query->result();
    }

    public function reportes_inmueble()
    {
        $this->db->select('SUM(Det_Pre_valor) valor, Inm_Id');
        $this->db->from('Detalle_Presupuesto');
        $this->db->group_by('Inm_Id');
        $query = $this->db->get();
        return $query->result();
    }
}

?>
