<?php
class ZonaComun_Model extends CI_Model {

    public function crear_zonacomun($datos){
        

        $this->db->insert('Zonas_Comunes', $datos);

        return true;
    }

    public function listar_zonacomun(){

        $this->db->select('Zon_Com_Id, Zon_Com_Nombre, Zon_Periodo_Mantenimiento, Zon_Descripcion');
        $this->db->from('Zonas_Comunes');
        $query = $this->db->get();

        return $query->result();
    }

    public function listar_zonacomunxId($id){

        $sql=$this->db->query("SELECT * FROM Zonas_Comunes WHERE Zon_Com_Id = $id");

        return $sql->result()[0];
    }
    
    public function borrar_zonacomun($id){
        $this->db->delete('Zonas_Comunes', array('Zon_Com_Id' => $id));
        //return true;
    }

    public function actualizar_zonacomun($datos, $id){
        
        $this->db->set($datos);
        $this->db->where('Zon_Com_Id', $id);
        $this->db->update('Zonas_Comunes');

        return true;
    }

    public function Buscar($BuscarDatos){
       
        $this->db->select('Zon_Com_Id, Zon_Com_Nombre, Zon_Periodo_Mantenimiento, Zon_Descripcion');
        $this->db->from('Zonas_Comunes');

        $this->db->like('Zon_Com_Nombre', $BuscarDatos['Zon_Com_Nombre']);
        $this->db->like('Zon_Periodo_Mantenimiento', $BuscarDatos['Zon_Periodo_Mantenimiento']);

        $sql=$this->db->get(); 
        return $sql->result();

    }

    public function Reportes() {

        $sql=$this->db->query("SELECT Z.Zon_Com_Nombre ZonaComun, COUNT(R.Res_Id) TotalReservas 
        FROM Zonas_Comunes Z
        INNER JOIN Reservas R ON Z.Zon_Com_Id = R.Zon_Com_Id
        GROUP BY Z.Zon_Com_Nombre");

        return $sql->result();

    }

    // public function ReportesDos() {

    //     $sql=$this->db->query("SELECT U.Usu_Nombres Usuarios, COUNT(R.Res_Id) TotalReservas 
    //     FROM Usuarios U
    //     INNER JOIN Reservas R ON U.Usu_Id = R.Usu_Id
    //     GROUP BY U.Usu_Nombres");

    //     return $sql->result();

    // }

    public function ReportesTres() {

        $sql=$this->db->query("SELECT Z.Zon_Com_Nombre ZonaComun, COUNT(M.Man_Id) TotalMantenimientos 
        FROM Zonas_Comunes Z
        INNER JOIN Mantenimiento M ON Z.Zon_Com_Id = M.Zon_Com_Id
        GROUP BY Z.Zon_Com_Nombre");

        return $sql->result();

    }
}
?>