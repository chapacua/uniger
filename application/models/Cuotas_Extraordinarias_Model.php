<?php
class Cuotas_Extraordinarias_Model extends CI_Model

{

    
    public function borrar_cuota_extraordinaria($periodo)
    {
        $this->db->select('Per_Periodo');
        $query = $this->db->get('Presupuestos');
        
        
        $this->db->where('Pre_Id', $periodo);
        $this->db->delete('Presupuestos');
        
        return $query->result()[0]->Cuo_Ext_Id;
    }

    public function borrar_detalles_extraordinaria($cuota_extraordinaria)
    {
        $this->db->where('Pre_Periodo', $cuota_extraordinaria);
        $this->db->delete('Detalle_Presupuesto');
    }

    
    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function traer_meses_disponibles()
    {
        if (date('m') == "12") {
            $this->db->select('*');    
            $query = $this->db->get('Mes');  
        }
        else
        { 
            $this->db->select('*');    
            $this->db->where('Mes_Id >', 'MONTH(CURDATE())', false);
        }
        
        $query = $this->db->get('Mes');

        return $query->result();
    }

    /**
     * Consulta los periodos mayores o iguales al periodo actual
     *
     * @param       array  $datos
     * @return      int $insert_id
     */   
    function traer_periodos()
    {
        if (date('m') == "12") {
            $this->db->select('Per_Periodo');
            $this->db->where('Per_Periodo >', 'YEAR(CURDATE())', false);
            $query = $this->db->get('Periodo');
        }
        else
        {
            $this->db->select('Per_Periodo');
            $this->db->where('Per_Periodo >=', 'YEAR(CURDATE())', false);
            $query = $this->db->get('Periodo');
        }

        return $query->result();
    }

    /**
     * Guarda el nuevo valor para ser recaudadopor concepto de cuota extraordinaria
     *
     * @param       array  $datos
     * @return      int $insert_id
     */   
    function guardar_cuota_extraordinaria($datos)
    {
    
        $this->db->insert('Cuotas_Extraordinarias', $datos);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    /**
     * Crea los detalles del cobro de cuota extraordinaria ques erá realizado por inmueble
     *
     * @param       array  $datos
     * @return      N/A
     */
    function guardar_detalle_cuota_extraordinaria($valor, $id_presupuesto, $mes, $descripcion)
    {
        $inmuebles = $this->inm_traer_datos_presupuesto();
        foreach ($inmuebles as $inmueble) 
        {
            $total_pagar = round($valor*$inmueble->Inm_Coeficiente_Propiedad);
            
            $datos = array (
                'Cuo_Ext_Id' => $id_presupuesto,
                'Inm_Id' => $inmueble->Inm_Id,
                'Mes_Id' => $mes,
                'Det_Cuo_Ext_Valor' => $total_pagar,
                'Det_Cuo_Ext_Descripcion' => $descripcion
            );

            $this->db->insert('Detalle_Cuotas_Extraordinarias', $datos);
        }        
    }

        
    /**
     * Crea los detalles del cobro mes a mes ques erá realizado por inmueble
	 *
     * @param       N/A
     * @return      array/object  $query->result()
	 */
    function inm_traer_datos_presupuesto()
    {
        
        $this->db->select('Inm_Id, Inm_Coeficiente_Propiedad');
        $query = $this->db->get('Inmuebles');
        return $query->result();
    }

    
    public function traer_detalles_cuotas_extraordinarias()
    {
        $this->db->select('d.*, i.Inm_Tipo, c.Per_Periodo, m.Mes_Nombre');
        $this->db->join('Inmuebles i', 'd.Inm_Id = i.Inm_Id', 'left');
        $this->db->join('Mes m', 'd.Mes_Id = m.Mes_Id', 'left');
        $this->db->join('Cuotas_Extraordinarias c', 'd.Cuo_Ext_Id = c.Cuo_Ext_Id', 'left');
        $this->db->group_by('Per_Periodo, Inm_Id');
        $query = $this->db->get('Detalle_Cuotas_Extraordinarias d');
 
        return $query->result();
    }
    
    public function traer_cuotas_extraordinarias()
    {
        $this->db->select('c.*, m.Mes_Nombre');
        $this->db->join('Mes m', 'c.Mes_Id = m.Mes_Id', 'left');
        $query = $this->db->get('Cuotas_Extraordinarias c');
        
        return $query->result();
    }

    /**
     *Valida si un campo tiene el formato de la fecha correctamente
     *
     * @param       date  $date
     * @return      true/false
    */
    function crear_recibos_pago($mes_actual, $inmuebleProcesar, $periodo_actual, $recibo_id){

        $this->db->select('d.Det_Cuo_Ext_Id');    
        $this->db->where('d.Mes_Id', $mes_actual);
        $this->db->where('d.Inm_Id', $inmuebleProcesar);
        $this->db->join('Cuotas_Extraordinarias c', 'd.Cuo_Ext_Id  = c.Cuo_Ext_Id');
        $this->db->join('Periodo p', 'p.Per_Periodo  = c.Per_Periodo');
        $this->db->where('p.Per_Periodo', $periodo_actual);
        $query = $this->db->get('Detalle_Cuotas_Extraordinarias d');

        foreach($query->result() as $row)
        {
            $datos = array(
                'Det_Cuo_Ext_Id' => $row->Det_Cuo_Ext_Id,
                'Det_Rec_Pag_Estado' => 'Por Imprimir',
                'Rec_Pag_Id' => $recibo_id
            );

            $this->db->insert('Detalle_Recibo_Pago', $datos);
            
        }
    }

    
    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function traer_cuotas_extraordinarias_por_recibo($Rec_Pag_Id)
    {
        $this->db->select('Rec_Pag_Id');
        $this->db->where('Rec_Pag_Pdf = ', $Rec_Pag_Id.".pdf");
        $query = $this->db->get('Recibo_Pago');
        
        $this->db->select('dce.`Det_Cuo_Ext_Valor`');
        $this->db->join('`Detalle_Cuotas_Extraordinarias` AS dce', 'd.`Det_Cuo_Ext_Id` = dce.`Det_Cuo_Ext_Id`');
        $this->db->join('Recibo_Pago AS rp', 'd.`Rec_Pag_Id` = rp.`Rec_Pag_Id`');
        $this->db->where('d.`Rec_Pag_Id` =', $query->result()[0]->Rec_Pag_Id);
        
        $query = $this->db->get('Detalle_Recibo_Pago AS d');

        return $query->result();
    }

    public function reporter_promedio()
    {
        $this->db->select('AVG(Det_Cuo_Ext_Valor) valor, Cuo_Ext_Id');
        $this->db->from('Detalle_Cuotas_Extraordinarias');
        $this->db->group_by('Cuo_Ext_Id');
        $query = $this->db->get();
        return $query->result();
    }
}

?>
