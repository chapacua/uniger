<?php
class Publicaciones_Model extends CI_Model {


    public function Listar_Publicaciones(){

        $this->db->select('Pub_Id, Pub_Fecha_Publicacion, Pub_Contenido,Pub_Tipo_Informacion,Pub_Imagen');
        $this->db->from('Publicaciones');
        $query = $this->db->get();

        return $query->result();
    
}
public function listar_publicacionesXId($id){

    $sql=$this->db->query("SELECT * FROM Publicaciones WHERE Pub_Id = $id");

    return $sql->result()[0];
}
public function Agregar_Publicacion($datos)
{
    
        $this->db->set('Pub_Fecha_Publicacion', 'NOW()', false);
        $this->db->insert('Publicaciones',$datos);

        return true;

}
public function Editar(){
     
    $this->db->set($datos);
    $this->db->where('Pub_Id', $id);
    $this->db->update('Publicaciones');

    return true;
}
public function Borrar_Pub($id){
    $this->db->delete('Publicaciones', array('Pub_Id' => $id));
    return true;
}
public function Reportes(){
    $sql=$this->db->query("SELECT COUNT(Pub_Id) AS numPublicaciones, Pub_Tipo_Informacion FROM Publicaciones GROUP BY Pub_Tipo_Informacion");

    return $sql->result();
}
}