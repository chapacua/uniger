<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="#">Inicio</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            
            <div class="row">

                    <!-- Col 1 -->
                    <div class="col-lg-6">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Información de facturación</h4>
                                <p class="text-muted m-b-30 font-14">Información sobre la configuración de facturación actual</p>

                                <form action="#">
                                    <div class="form-group">
                                        <label>Periodo actual:</label>
                                        <input type="text" class="colorpicker-default form-control colorpicker-element" value="<?php echo date('Y'); ?>" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label>Mes actual:</label>
                                        <input type="text" class="colorpicker-default form-control colorpicker-element" value="<?php echo date('m'); ?>" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label>Inmuebles registrados:</label>
                                        <input type="text" class="colorpicker-default form-control colorpicker-element" value="<?php echo date('m'); ?>" disabled>
                                    </div>
                                </form>

                            </div>
                        </div>

                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Cuotas extraordinarias</h4>
                                <p class="text-muted m-b-30 font-14">Gestión de cuotras extraordinarias.</p>
                                <?php $args= array('id' => 'conf_crear_cuota_extraordinaria'); ?>
                                <?php echo form_open('Recibos/conf_crear_cuota_extraordinaria', $args); ?>
                                    <div class="form-group">
                                        <label for="input-presupuesto">Valor a recaudar (COP)*</label>
                                        <input class="form-control input-val" name="input-presupuesto" id="input-presupuesto" required>
                                        <div id="error"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-periodo">Periodo*</label>
                                        <div>
                                        <select class="selectpicker col-lg-12" name="input-periodo" id="input-periodo" data-live-search="true" title="Seleccione un periodo">
                                            <?php foreach ($periodos as $periodo) { ?>
                                                <option data-tokens="<?php echo $periodo->Per_Periodo; ?>"><?php echo $periodo->Per_Periodo; ?></option>
                                            <?php } ?>
                                        </select>
                                        </div>
                                        <div id="error"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-mes">Mes*</label>
                                        <div>
                                        <select class="form-control col-lg-12" name="input-mes" id="input-mes" title="Seleccione un periodo">
                                            <?php foreach ($meses as $mes) { ?>
                                                <option value="<?php echo $mes->Mes_Id; ?>"><?php echo $mes->Mes_Nombre; ?></option>
                                            <?php } ?>
                                        </select>
                                        </div>
                                        <div id="error"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-descripcion">Descripción cuota extraordinaria*</label>
                                        <textarea name="input-descripcion" id="input-descripcion" class="form-control" rows="5" placeholder="Describa el presupuesto a ser generado"></textarea>
                                        <div id="error"></div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success" id="submitBtnCuota">Crear Cuota Extraordinaria</button>
                                        <div class="loader-container">
                                        </div>
                                        <button class="btn btn-success d-none" disabled id="loaderCuota"><span class="mr-2">Creando, por favor espere...</span><div class="loader"></div></button>
                                    </div>

                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div> <!-- end col 1 -->

                    <!-- Col 2 -->
                    <div class="col-lg-6">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Administración</h4>
                                <p class="text-muted m-b-30 font-14">Gestión del presupuestos anuales.</p>
                                <?php $args= array('id' => 'conf_crear_presupuesto'); ?>
                                <?php echo form_open('Recibos/conf_crear_presupuesto', $args); ?>
                                    <div class="form-group">
                                        <label for="input-presupuesto">Valor presupuesto (COP)*</label>
                                        <input class="form-control input-val" name="input-presupuesto" id="input-presupuesto" required>
                                        <div id="error"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-periodo">Periodo*</label>
                                        <div>
                                        <select class="selectpicker col-lg-12" name="input-periodo" id="input-periodo" data-live-search="true" title="Seleccione un periodo">
                                            <?php foreach ($periodos_presupuesto as $periodo) { ?>
                                                <option data-tokens="<?php echo $periodo->Per_Periodo; ?>"><?php echo $periodo->Per_Periodo; ?></option>
                                            <?php } ?>
                                        </select>
                                        </div>
                                        <div id="error"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-descripcion">Descripción presupuesto*</label>
                                        <textarea name="input-descripcion" id="input-descripcion" class="form-control" rows="5" placeholder="Describa la razón de la cuota extraordinaria"></textarea>
                                        <div id="error"></div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success " id="submitBtn">Crear presupuesto</button>
                                        <div class="loader-container">
                                        
                                        </div>
                                        <button class="btn btn-success d-none" disabled id="loader"><span class="mr-2">Creando, por favor espere...</span><div class="loader"></div></button>
                                    </div>

                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>


        </div><!-- Fin container -->
    </div>
    <!-- Fin wrapper -->
    <?php $this->load->view('footer'); ?>