<?php $this->load->view('header'); ?>

       
        <!-- App css -->
        <link href="<?php echo base_url('application/views/'); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('application/views/'); ?>assets/css/FullCalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo base_url('application/views/'); ?>assets/js/FullCalendar/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('application/views/'); ?>assets/js/FullCalendar/moment.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">UNIGER</a></li>
                                    <li class="breadcrumb-item active">Calendario</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Calendario</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-xl-1 col-lg-1 col-md-1"></div>
                                    <div id='calendar' class="col-xl-10 col-lg-10 col-md-10" style="overflow: auto;"></div>
                                    <div class="col-xl-1 col-lg-1 col-md-1"></div>
                                </div>
                                <!-- end row -->

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->



                <!-- MODAL INSERT -->
                <div class="modal fade" id="ModalIngresoReservas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Ingreso de reserva</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-xl-1 col-lg-1 col-md-1"></div>
                                    <div class="col-xl-10 col-lg-10 col-md-10">
                                        <div class="form-group">
                                            <label for="Cantidad">Cantidad de personas</label>
                                            
                                            <input type="text" class="form-control"  placeholder="Cantidad" name="Res_Cantidad_Personas" id="Res_Cantidad_Personas">
                                        </div>

                                        <div class="form-group">
                                        <label for="Estado">
                                                <b>Estado de la reserva</label>
                                            <select class="form-control" name="Res_Estado" id="Res_Estado">
                                                <option>Reservado</option>
                                                <option>No reservado</option>
                                                <option>Pendiente</option>
                                            </select> 

                                        </div> 
                                        <div class="form-group">
                                            <label for="tipoevento">
                                                <b>Tipo de evento</label>

                                                <input type="text" class="form-control"  placeholder="Tipo de evento" name="Res_Tipo_Evento" id="Res_Tipo_Evento">
                                            <!-- <select class="form-control" name="Res_Tipo_Evento" id="Res_Tipo_Evento">
                                                <option>Cumpleaños</option>
                                                <option>Reunion familiar</option>
                                                <option>Baby shower</option>
                                            </select> -->
                                            <input type="text" class="form-control" name="FechaInicio" id="FechaInicio" hidden>
                                            <input type="text" class="form-control" name="FechaFinal" id="FechaFinal" hidden>
                                        </div>
                                        
                                    </div>
                                    <div class="col-xl-1 col-lg-1 col-md-1"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary" onclick="GuardarReserva();">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- MODAL UPDATE -->
                <div class="modal fade" id="ModalUpdateReservas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Modificación de reserva</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-xl-1 col-lg-1 col-md-1"></div>
                                    <div class="col-xl-10 col-lg-10 col-md-10">
                                        <div class="form-group">
                                            <label for="modCantidad">Cantidad de personas</label>
                                            
                                            <input type="text" class="form-control"  placeholder="Cantidad" name="mod_Cantidad_Personas" id="mod_Cantidad_Personas">
                                        </div>
                                        <div class="form-group">
                                        <label for="modEstado">
                                                <b>Estado de la reserva</label>
                                            <select class="form-control" name="mod_Estado" id="mod_Estado">
                                                <option>Reservado</option>
                                                <option>No reservado</option>
                                                <option>Pendiente</option>
                                            </select>

                                        </div>
                                        <div class="form-group">
                                            <label for="modtipoevento">
                                                <b>Tipo de evento</label>

                                                 <input type="text" class="form-control"  placeholder="Tipo de evento" name="mod_Tipo_Evento" id="mod_Tipo_Evento">

                                            <input type="text" class="form-control" name="TokenMod" id="TokenMod" hidden>
                                            <input type="text" class="form-control" name="mod_FechaInicial" id="mod_FechaInicial" hidden>
                                            <input type="text" class="form-control" name="mod_FechaFinal" id="mod_FechaFinal" hidden>
                                            
                                        </div>
                                        
                                    </div>
                                    <div class="col-xl-1 col-lg-1 col-md-1"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary" onclick="EditarReserva();">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>


                 <!-- MODAL DELETE -->
                 <div class="modal fade" id="ModalDeleteReservas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Eliminación</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-xl-1 col-lg-1 col-md-1"></div>
                                    <div class="col-xl-10 col-lg-10 col-md-10">
                                        <div class="form-group">
                                            <p>¿Está seguro que desea eliminar la reserva?</p>
                                        </div>
                                        
                                            <input type="text" class="form-control" name="TokenDel" id="TokenDel" hidden>
                                          
                                    </div>
                                    <div class="col-xl-1 col-lg-1 col-md-1"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary" onclick="EliminarReserva();">Aceptar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </div>
        <input type="text" class="form-control" name="jsonResponse" id="jsonResponse" hidden>
                                          

<?php $this->load->view('footer'); ?>


        <!-- Jquery-Ui -->
        
        <script type="text/javascript" src="<?php echo base_url('application/views/'); ?>assets/js/FullCalendar/fullcalendar.min.js"></script>
        
        <!-- App js -->
        <!-- <script type="text/javascript" src="<?php echo base_url('application/views/'); ?>assets/js/app.js"></script> -->

        <script type="text/javascript">

        
        function GuardarReserva(){
            var CantPer = document.getElementById("Res_Cantidad_Personas");
            var EstRes = document.getElementById("Res_Estado");
            var TipoEven = document.getElementById("Res_Tipo_Evento");
            var Fechaini = document.getElementById("FechaInicio");
            var FechaFi = document.getElementById("FechaFinal");
            
            var FToken = new Date();

            var Token = (FToken.getMonth()+1) +''+FToken.getDate() + '' + FToken.getHours() +''+ FToken.getMilliseconds();

            var Registro = {
                "Res_Cantidad_Personas": CantPer.value,
                "Res_Estado": EstRes.value,
                "Res_Tipo_Evento": TipoEven.value,
                "Res_Fecha_Inicio": Fechaini.value,
                "Res_Fecha_Fin": FechaFi.value,
                "Res_Token": Token
            };

            var FechaI = Fechaini.value.split('-');
            // var SFechaI = FechaI[0] + '-' + FechaI[1] + '-' + FechaI[2];
            var DFechaI = new Date(FechaI[0], FechaI[1]-1, FechaI[2], 23, 59, 00);
            
            if (DFechaI < FToken){
                swal("No es posible reservar en días anteriores al día actual !");
                return;
            }
            console.log(typeof(CantPer.value));
            if (CantPer.value == ''){
                swal("El campo cantidad de personas es obligatorio");
                return;
            }

            if (isNaN(CantPer.value)){
                swal("El campo cantidad de personas solo puede contener numeros");
                return;
            }
            
            if (EstRes.value == ''){
                swal("El campo estado de la reserva es obligatorio");
                return;
            }

             if (TipoEven.value == ''){
                swal("El campo tipo de evento es obligatorio");
                return;
            }
            

            $.ajax({
                url: '<? echo base_url('Reservas/crear_reserva');?>',
                method: 'POST',
                dataType: 'json',
                data: Registro,
                success: function(response)
                {

                    var calendar = $('#calendar').fullCalendar('getCalendar');

                    var FechaIAntes = response.Res_Fecha_Inicio.split('-');
                    var FechaFAntes = response.Res_Fecha_Fin.split('-');
                    
                    if (FechaIAntes[2]<10){
                        FechaIAntes[2] = '0'+FechaIAntes[2]
                    }
                    if (FechaFAntes[2]<10){
                        FechaFAntes[2] = '0'+FechaFAntes[2]
                    }

                    //Segmento para fecha inicial
                    var FechaInicial = new Date(FechaIAntes);
                    
                    var monthi = FechaInicial.getMonth()+1;
                    if(monthi<10){
                        monthi='0'+monthi;
                    } 
                    var dayi = FechaInicial.getDate();
                    if(dayi<10){
                        dayi='0'+dayi;
                    } 
                    var yeari = FechaInicial.getFullYear();
                    
                    //Segmento para fecha final
                    var FechaFinal = new Date(FechaFAntes);
                    
                    var monthf = FechaFinal.getMonth()+1;
                    if(monthf<10){
                        monthf='0'+monthf;
                    } 
                    var dayf = FechaFinal.getDate();
                    if(dayf<10){
                        dayf='0'+dayf;
                    } 
                    var yearf = FechaFinal.getFullYear();

                    eventData = {
                        id : response.Res_Token,
                        title: response.Res_Tipo_Evento,
                        start: yeari + '-' + monthi + '-' + dayi,
                        end: yearf + '-' + monthf + '-' + dayf
                    };

                    $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                    $('#calendar').fullCalendar('unselect'); 

                    swal("Reserva Creada Exitosamente !");
                    $('#ModalIngresoReservas').modal('hide');

                    document.getElementById("Res_Cantidad_Personas").value = '';
                    document.getElementById("Res_Estado").value = '';
                    document.getElementById("Res_Tipo_Evento").value = 'Cumpleaños';
                    document.getElementById("FechaInicio").value = '';
                    document.getElementById("FechaFinal").value = '';
                },
                error: function (err) {
                    alert("Ocurrió un error: " + JSON.stringify(err, null, 2));
                }
            });
        }

        function EditarReserva(){

            var CantPerMod = document.getElementById("mod_Cantidad_Personas");
            var EstResMod = document.getElementById("mod_Estado");
            var TipoEvenMod = document.getElementById("mod_Tipo_Evento");
            var FechaInicial = document.getElementById("mod_FechaInicial");
            var FechaFinal = document.getElementById("mod_FechaFinal");
            var TokenMod = document.getElementById("TokenMod");
            
            
            var Registro = {
                "Res_Cantidad_Personas": CantPerMod.value,
                "Res_Estado": EstResMod.value,
                "Res_Tipo_Evento": TipoEvenMod.value,
                "Res_FechaInicial": FechaInicial.value,
                "Res_FechaFinal": FechaFinal.value,
                "Res_Token": TokenMod.value
            };

            $.ajax({
                url: '<? echo base_url('Reservas/Editar_Reserva');?>',
                method: 'POST',
                dataType: 'json',
                data: Registro,
                success: function(response)
                {
                    $('#calendar').fullCalendar('removeEvents',response.Res_Token);

                    //Segmento para fecha inicial
                    var FechaModInicial = new Date(response.Res_FechaInicial);
                    
                    var monthim = FechaModInicial.getMonth()+1;
                    if(monthim<10){
                        monthim='0'+monthim;
                    } 
                    var dayim = FechaModInicial.getDate() ;
                    if(dayim<10){
                        dayim='0'+dayim;
                    } 
                    var yearim = FechaModInicial.getFullYear();
                    
                    //Segmento para fecha final
                    var FechaModFinal = new Date(response.Res_FechaFinal);
                    
                    var monthfm = FechaModFinal.getMonth()+1;
                    if(monthfm<10){
                        monthfm='0'+monthfm;
                    } 
                    var dayfm = FechaModFinal.getDate();
                    if(dayfm<10){
                        dayfm='0'+dayfm;
                    } 
                    var yearfm = FechaModFinal.getFullYear();

                    eventData = {
                        id : response.Res_Token,
                        title: response.Res_Tipo_Evento,
                        start: yearim + '-' + monthim + '-' + dayim,
                        end: yearfm + '-' + monthfm + '-' + dayfm
                    };

                    $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                    $('#calendar').fullCalendar('unselect'); 
                    
                    swal("Reserva Editada Exitosamente !");
                    $('#ModalUpdateReservas').modal('hide');

                    document.getElementById("mod_Cantidad_Personas").value = '';
                    document.getElementById("mod_Estado").value = '';
                    document.getElementById("mod_Tipo_Evento").value = 'Cumpleaños';
                    document.getElementById("mod_FechaInicial").value = '';
                    document.getElementById("mod_FechaFinal").value = '';
                },
                error: function (err) {
                    alert("Ocurrió un error: " + JSON.stringify(err, null, 2));
                }
            });
        }

        function EliminarReserva(){

            var TokenDel = document.getElementById("TokenDel");

            var Registro = {
                "Res_Token": TokenDel.value
            };

            $.ajax({
                url: '<? echo base_url('Reservas/Delete_Reserva');?>',
                method: 'POST',
                dataType: 'json',
                data: Registro,
                success: function(response)
                {
                    $('#calendar').fullCalendar('removeEvents',response.Res_Token);
                    $('#ModalDeleteReservas').modal('hide');

                },
                error: function (err) {
                    alert("Ocurrió un error: " + JSON.stringify(err, null, 2));
                }
            });
            }
        
        $(document).ready(function() {

            var dt = new Date();

            // Display the month, day, and year. getMonth() returns a 0-based number.
            var month = dt.getMonth()+1;
            if(month<10){
                month='0'+month;
            } 
            var day = dt.getDate();
            var year = dt.getFullYear();

            var fechaactual = year + '-' + month + '-' + day;
            
            
            $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                //center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            events: function(start, end, timezone, callback){
                $.ajax({
                    url: '<? echo base_url('Reservas/FindAll_Reserva');?>',
                    method: 'POST',
                    dataType: 'json',
                    success: function(response)
                    {   
                       callback(response);
                    },
                    async: false
                });
            },
            defaultDate: fechaactual,
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectHelper: true,
            select: function(start, end) {
                
                $('#ModalIngresoReservas').modal('show');
                var Finicio = new Date(start);
                // Display the month, day, and year. getMonth() returns a 0-based number.
                var monthi = Finicio.getMonth()+1;
                if(monthi<10){
                    monthi='0'+monthi;
                } 
                var dayi = Finicio.getDate() + 1;
                var yeari = Finicio.getFullYear();
                
                document.getElementById("FechaInicio").value = yeari + '-' + monthi + '-' + dayi;
                document.getElementById("FechaFinal").value = yeari + '-' + monthi + '-' + dayi;

            },
            eventRender: function(event, element) {
                element.append( "<i style='margin: 8px;' class='fa fa-remove closeon' aria-hidden='true'></i>" );
                element.append( "<i style='margin: 8px;' class='fa fa-pencil modon' aria-hidden='true'></i>" );
                element.find(".closeon").click(function() {

                    document.getElementById("TokenDel").value = event.id;
                    $('#ModalDeleteReservas').modal('show');
                    
                });
                
                element.find(".modon").click(function() {
                   
                    var Token = event.id;

                    var Registro = {
                        "Res_Token": Token
                    };
                    
                    $.ajax({
                        url: '<? echo base_url('Reservas/Find_Reserva');?>',
                        method: 'POST',
                        dataType: 'json',
                        data: Registro,
                        success: function(response)
                        {
                            document.getElementById("mod_Cantidad_Personas").value = response[0].Res_Cantidad_Personas;
                            document.getElementById("mod_Estado").value = response[0].Res_Estado;
                            document.getElementById("mod_Tipo_Evento").value = response[0].Res_Tipo_Evento;
                            document.getElementById("mod_FechaInicial").value = response[0].Res_Fecha_Inicio;
                            document.getElementById("mod_FechaFinal").value = response[0].Res_Fecha_Fin;
                            document.getElementById("TokenMod").value = Token;
                            
                            $('#ModalUpdateReservas').modal('show');
                        },
                        error: function (err) {
                            alert("Ocurrió un error: " + JSON.stringify(err, null, 2));
                        }
                    });

                });
            },
            editable: true,
            eventLimit: true, // allow "more" link when too many events
          
            });

        });
            

        </script>