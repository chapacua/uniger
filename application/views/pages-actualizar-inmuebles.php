<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- container -->
    <div class="container-fluid">
        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url('Inmuebles'); ?>">Inmuebles</a>
                            </li>
                            <li class="breadcrumb-item active">
                                <a href="<?php echo base_url('Inmuebles/listar'); ?>">Consultar inmueble</a>
                            </li>
                        </ol>
                    </div>
                    <h4 class="page-title">Información inmueble</h4>
                </div>
            </div>
        </div>
        <!-- Fin titulo y mida de pan -->
        <div class="card m-b-30">
            <div class="card-body">
                <h3 class="m-t-0">
                    <img src="<?php echo base_url('application/views/'); ?>assets/images/logo-dark.png" alt="logo" height="32" />
                </h3>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="p-20">
                            <?php echo form_open('inmuebles/actualizar_inmueble');?>
                            <div class="form-group">
                                <label>Tipo de inmueble </label>
                                <select class="form-control" id="" name="Inm_Tipo">
                                    <?php
                                     if($informacion->Inm_Tipo == "Apartamento"){?>
                                    <option value="Apartamento">Apartamento</option>
                                    <option value="Cuarto Util">Cuarto Util</option>
                                    <option value="Parqueadero">Parqueadero</option>
                                    <option value="Apartaestudio">Apartaestudio</option>
                                    <?php
                                    }elseif($informacion->Inm_Tipo == "Cuarto Util"){?>
                                    <option value="Cuarto Util">Cuarto Util</option>
                                    <option value="Apartamento">Apartamento</option>
                                    <option value="Parqueadero">Parqueadero</option>
                                    <option value="Apartaestudio">Apartaestudio</option>
                                    <?php
                                    }elseif($informacion->Inm_Tipo == "Parqueadero"){?>
                                    <option value="Parqueadero">Parqueadero</option>
                                    <option value="Cuarto Util">Cuarto Util</option>
                                    <option value="Apartamento">Apartamento</option>
                                    <option value="Apartaestudio">Apartaestudio</option>
                                    <?php 
                                         }else{
                                        ?>
                                    <option value="Apartaestudio">Apartaestudio</option>
                                    <option value="Parqueadero">Parqueadero</option>
                                    <option value="Cuarto Util">Cuarto Util</option>
                                    <option value="Apartamento">Apartamento</option>
                                    <?php } ?>
                                </select>
                                <?php echo form_error('Inm_Tipo'); ?>
                            </div>
                            <div class="form-group">
                                <label>Dirección</label>
                                <input type="text" value="Torres de la giralda" class="form-control" name="Inm_Direccion" disabled> </div>
                                <div class="form-group">
                                <label>Propietario</label>
                                  <input type="text" placeholder="" name="Usu_Id" value="<?php echo $informacion->Usu_Nombres." ".$informacion->Usu_Apellidos; ?>" class="form-control" disabled>
                                     <?php echo form_error('Usu_Id'); ?>
                            </div>
                            <div class="form-group">
                                <label>Número de inmueble</label>
                                <input type="text" placeholder="" value="<?php echo $informacion->Inm_Id; ?>" class="form-control" name="Imd_id" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="p-20">
                            <div class="form-group">
                                <label>Número de matrícula</label>
                                <input type="text" placeholder="" value="<?php echo $informacion->Inm_Numero_Matricula; ?>" class="form-control" name="Inm_Numero_Matricula">
                                <?php echo form_error('Inm_Numero_Matricula'); ?>
                            </div>
                            <div class="form-group">
                                <label>Área construida</label>
                                <input type="text" placeholder="" value="<?php echo $informacion->Inm_Area_Construida; ?>" class="form-control" name="Inm_Area_Construida">
                                <?php echo form_error('Inm_Area_Construida'); ?>
                            </div>
                            <div class="form-group">
                                <label>Área privada</label>
                                <input type="text" placeholder="" value="<?php echo $informacion->Inm_Area_Privada; ?>" class="form-control" name="Inm_Area_Privada">
                                <?php echo form_error('Inm_Area_Privada'); ?>
                            </div>
                            <div class="form-group">
                                <label>Coeficiente de propiedad</label>
                                <input type="number" step="any" placeholder="" value="<?php echo $informacion->Inm_Coeficiente_Propiedad; ?>" class="form-control"
                                    name="Inm_Coeficiente_Propiedad">
                                <?php echo form_error('Inm_Coeficiente_Propiedad'); ?>
                            </div>
                            <input type="hidden" name="inmueble_id" value="<?php echo $id; ?>">
                        </div>
                    </div>
                    <div style="left: 200px; margin: 0 auto;">
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                        <a href="<?php echo base_url("inmuebles/editar_usu_inmueble/".$informacion->Inm_Id);?>" class="btn btn-primary">
                            Agregar usuario
                        </a>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>

        </div>

        <br>
        <?php $this->load->view('footer'); ?>