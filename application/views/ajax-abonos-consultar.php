    <?php if(!empty($abonos)){ ?>
    <table class="table table-striped" id="datatable">
        <thead>
        <tr>
            <th>Valor</th>
            <th>Fecha</th>
            <th>Eliminar</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($abonos as $abono) { ?>
            <tr id="row-<?php echo $abono->Abo_Id; ?>">
                <td><?php echo number_format($abono->Abo_Valor); ?></td>
                <td><?php echo $abono->Abo_Fecha; ?></td>
                <td>                                            
                    <a href="#" class="btn btn-outline-danger waves-effect waves-light mr-2 borrarAbono" id="pre-<?php echo $abono->Abo_Id; ?>"><i class=" mdi mdi-delete"></i></a>
                    <button class="btn btn-outline-danger waves-effect waves-light mr-2 d-none" disabled id="delete-<?php echo $abono->Abo_Id; ?>"><div class="loader"></div></button></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?php }else{ ?>
    <h5>No se han realizado abonos a este recibo...</h5>
    <?php } ?>
