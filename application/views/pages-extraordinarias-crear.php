<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row ">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('recibos'); ?>">Inicio recibos de pago</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('Cuotas_Extraordinarias'); ?>">Inicio Cuotas Extraordinarias</a>
                                </li>
                                <li class="breadcrumb-item active">Cuotas Extraordinarias</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Cuotas Extraordinarias</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            
            <div class="row">

                    <!-- Col -->
                    <div class="col-lg-6 offset-lg-3">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Cuotas extraordinarias</h4>
                                <p class="text-muted m-b-30 font-14">Gestión de cuotas extraordinarias.</p>
                                <?php $args= array('id' => 'crear_cuota_extraordinaria'); ?>
                                <?php echo form_open('Cuotas_Extraordinarias/guardar', $args); ?>
                                    <div class="form-group">
                                        <label for="input-presupuesto">Valor a recaudar (COP)*</label>
                                        <input class="form-control input-val" name="input-presupuesto" id="input-presupuesto" required value="<?php echo set_value('input-valor'); ?>">
                                        <?php echo form_error('input-presupuesto'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-periodo">Periodo*</label>
                                        <div>
                                        <select class="selectpicker col-lg-12" name="input-periodo" id="input-periodo" data-live-search="true" title="Seleccione un periodo" required>
                                            <?php foreach ($periodos as $periodo) { ?>
                                                <option data-tokens="<?php echo $periodo->Per_Periodo; ?>"><?php echo $periodo->Per_Periodo; ?></option>
                                            <?php } ?>
                                        </select>
                                        </div>
                                        <?php echo form_error('input-periodo'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-mes">Mes*</label>
                                        <div>
                                        <select class="form-control col-lg-12" name="input-mes" id="input-mes" title="Seleccione un periodo" required>
                                            <?php foreach ($meses as $mes) { ?>
                                                <option value="<?php echo $mes->Mes_Id; ?>"><?php echo $mes->Mes_Nombre; ?></option>
                                            <?php } ?>
                                        </select>
                                        </div>
                                        <?php echo form_error('input-mes'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-descripcion">Describa cuota extraordinaria*</label>
                                        <textarea name="input-descripcion" id="input-descripcion" class="form-control" rows="5" placeholder="Describa el motivo por el cual se genera la cuota" required><?php echo set_value('input-valor'); ?></textarea>
                                        <?php echo form_error('input-descripcion'); ?>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-success" id="submitCuota">Crear Cuota Extraordinaria</button>
                                        <div class="loader-container">
                                        </div>
                                        <button class="btn btn-success d-none" disabled id="loaderCuota"><span class="mr-2">Creando, por favor espere...</span><div class="loader"></div></button>
                                    </div>

                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>


        </div><!-- Fin container -->
    </div>
    <!-- Fin wrapper -->
    <?php $this->load->view('footer'); ?>