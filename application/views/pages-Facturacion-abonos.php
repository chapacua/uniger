<?php $this->load->view('header'); ?>
        <!-- Fin MENU -->

        <!-- wrapper -->
        <div class="wrapper">
            <!-- Contenedor -->
            <div class="container-fluid">

                <!-- Titulo Página -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item"><a href="<?php echo base_url("/inicio");?>">Inicio</a></li>
                                    <li class="breadcrumb-item"><a href="<?php echo base_url("/recibos");?>">Recibos de pago</a></li>
                                    <li class="breadcrumb-item"><a href="<?php echo base_url("/recibos/Listar_recibos");?>">Recibos de pago</a></li>
                                    <li class="breadcrumb-item active">Abonos</li>
                                </ol>
                            </div>

                            <h4>Resultados</h4>
                        </div>
                    </div>
                </div>
                <div class="card m-b-30">
                    <div class="card-header">
                        <label>Filtro</label>
                    </div>
                    <div class="card-body">
                      <div class="form-group row">
                        <div class="col-md-4">
                        <label>Nombre Copropietario</label>
                      </div>
                      <div class="col-md-4">
                        <label>Numero del Apartamento</label>
                      </div>
                      <div class="col-md-4">
                        <label>Estado del Recibo</label>
                      </div>
                      </div>
                      <div class="form-group row">
                          <div class="col-md-4">
                              <input class="form-control" type="text" placeholder="Nombre Copropietario">
                          </div>
                          <div class="col-md-4">
                              <input class="form-control" type="number" placeholder="Número del Apartamento">
                          </div>
                          <div class="col-sm-4">
                              <select class="form-control" name="">
                                  <option value="">Estado del Recibo</option>
                                  <option value="">Moroso</option>
                                  <option value="">Pago</option>
                                  <option value="">Pendiente</option>
                              </select>
                          </div>
                      </div>

                      <div class="row">
                        <div class="offset-md-2 col-md-4">
                          <div class="form-group">
                              <label>Fecha emisión (desde)</label>
                              <div>
                                  <div class="input-group">
                                      <input type="text" class="form-control datepicker-autoclose" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                      <div class="input-group-append bg-custom b-0"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span></div>
                                  </div><!-- input-group -->
                              </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                              <label>Fecha emisión (hasta)</label>
                              <div>
                                  <div class="input-group">
                                      <input type="text" class="form-control datepicker-autoclose" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                      <div class="input-group-append bg-custom b-0"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span></div>
                                  </div><!-- input-group -->
                              </div>
                          </div>
                        </div>
                      </div>
                      <div class="offset-md-6">
                        <button type="button" class="btn btn-outline-secondary waves-effect">Filtrar<i class="mdi mdi-filter-outline"></i></button>
                      </div>
                    </div>
                </div>
                <!-- Fin titulo pagina y miga de pan -->
                <!-- Contenido Principal -->
                <div class="row">
                    <!-- Tabla -->
                    <table class="table table-striped">
                        <!-- Encabezado -->
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre de usuario</th>
                                <th>Apartamento</th>
                                <th>Fecha generacion de recibo</th>
                                <th>Valor abonos</th>
                                <th>Valor pendiente</th>
                                <th>Valor total</th>
                                <th>Acciones</th>
                            </tr>
                        </thead><!-- Fin Encabezado -->
                        <!-- Cuerpo -->
                        <tbody>
                            <tr>
                                <th scope="row">001</th>
                                <td>Santiago</td>
                                <td>105</td>
                                <td>18/11/2018</td>
                                <td>80.000</td>
                                <td>80.000</td>
                                <td>80.000</td>
                                <td><button type="button" class="btn btn-outline-info waves-effect waves-light mr-2"><i class="mdi mdi-eye"></i></button>
                                  <button type="button" class="btn btn-outline-warning waves-effect waves-light mr-2">
                                      <i class="ion-edit"></i>
                                  </button>
                                    <button href="javascript:window.print()" type="button" class="btn btn-outline-success waves-effect waves-light mr-2"><i class="mdi mdi-printer"></i></button>

                                </td>
                            </tr>
                        </tbody><!-- Fin Cuerpo -->
                    </table><!-- Fin Tabla -->
               </div>
            </div> <!-- fin container -->
        </div>
        <!-- Fin wrapper -->


        <?php $this->load->view('footer'); ?>