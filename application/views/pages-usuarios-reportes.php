<?php $this->load->view('header'); ?>


<!--Chartist Chart CSS -->
<link rel="stylesheet" href="<?php echo base_url('application/views/'); ?>assets/plugins/chartist/css/chartist.min.css">
<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Publicaciones</a>
                            </li>
                            <li class="breadcrumb-item active">Reportes</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Reportes</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-lg-6">
            <div class="card m-b-30">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Registros y aprobaciones</h4>
                        <p class="text-muted m-b-30 font-14 d-inline-block w-100">
                            En esta ayuda visual usted encontrará la cantidad de usuarios aceptados, rechazados y pendientes por aceptación dentro del sistema
                        </p>

                        <ul class="list-inline widget-chart m-t-20 m-b-15 text-center">
                        <?php 
                            foreach ($datoAprobaciones as $datosRol) {?>
                               <?php $porcentaje = ($datosRol->conteoEstados * 100 / $datoTotalAprobaciones->conteoTotalEstados); ?>
                               <li>
                                   <h4 class=""><b><?php echo $porcentaje; ?>%</b></h4>
                                   <p class="text-muted"><?php echo $datosRol->Reg_Estado; ?></p>
                               </li>
                        <?php } ?>
                        </ul>

                        <ul class="list-inline widget-chart m-t-20 m-b-15 text-center">
                        <?php 
                            foreach ($datoAprobaciones as $datosRol) {?>
                               <li>
                                   <h4 class=""><b><?php echo $datosRol->conteoEstados; ?></b></h4>
                                   <p class="text-muted">Usuario(s)</p>
                               </li>
                        <?php } ?>
                        </ul>
                        <ul class="list-inline widget-chart m-t-20 m-b-15 text-center">
                            <h3>Total usuarios registrados</h3>
                               <li>
                                   <h4 class=""><b><?php echo $datoTotalAprobaciones->conteoTotalEstados; ?></b></h4>
                                   <p class="text-muted">Usuarios</p>
                               </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card m-b-30">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Población de usuarios</h4>
                        <p class="text-muted m-b-30 font-14 d-inline-block w-100">
                            En este apartado se muestran los diferentes tipos de perfiles de usuarios registrados en la plataforma y su distribución
                        </p>

                        <ul class="list-inline widget-chart m-t-20 m-b-15 text-center">
                        <?php 
                            foreach ($datosRoles as $datosRol) {?>
                               <?php $porcentaje = ($datosRol->conteoCategorias * 100 / $datoTotalRoles->conteoTotalCategorias); ?>
                               <li>
                                   <h4 class=""><b><?php echo $porcentaje; ?>%</b></h4>
                                   <p class="text-muted"><?php echo $datosRol->InmXUsu_Rol; ?></p>
                               </li>
                        <?php } ?>
                        </ul>
                    </div>
                </div>
            </div> <!-- end col -->
            <!-- end col -->
        </div>
    </div>
</div>
<!-- end col -->
</div>
</div>
<!-- end container -->
</div>
<!-- end wrapper -->
<?php $this->load->view('footer'); ?>

<!--C3 Chart-->
<script type="text/javascript" src="<?php echo base_url('application/views/'); ?>assets/plugins/d3/d3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('application/views/'); ?>assets/plugins/c3/c3.min.js"></script>
<!-- <script src="<?php echo base_url('application/views/'); ?>assets/pages/c3-chart-init.js"></script> -->
<!--Chartist Chart-->
<script src="<?php echo base_url('application/views/'); ?>assets/plugins/chartist/js/chartist.min.js"></script>
<script src="<?php echo base_url('application/views/'); ?>assets/plugins/chartist/js/chartist-plugin-tooltip.min.js"></script>
<script src="<?php echo base_url('application/views/'); ?>assets/pages/chartist.init.js"></script>

<script>
    ! function (e) {
        "use strict";
        var a = function () { };
        a.prototype.init = function () {
            c3.generate({
                bindto: "#grafico1",
                data: {
                    columns: [
                        <?php 
                            foreach ($datosRoles as $datosRole) {?>
                               ["<?php echo $datosRole->conteoCategorias; ?>",<?php echo $datosRole->InmXUsu_Rol; ?> ],
                        <?php } ?>
                    ],
        type: "bar"
    },
        color: {
        pattern: ['#1f77b4', '#69f0ae']
    },
    axis: {
        y: {
            min: 0,
                tick: {
                format: d3.format('d')
            },
            padding: { top: 0, bottom: 0 }
        }
    }
            })
        }, e.ChartC3 = new a, e.ChartC3.Constructor = a
    }(window.jQuery),
        function (e) {
            "use strict";
            e.ChartC3.init()
        }(window.jQuery);
</script>