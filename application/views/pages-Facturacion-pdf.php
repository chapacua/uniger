


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                                    <li class="breadcrumb-item"><a href="#">Facturacion</a></li>
                                    <li class="breadcrumb-item"><a href="#">Recibos de pago</a></li>
                                    <li class="breadcrumb-item active">Recibo</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Recibo de pago</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-12">
                                         <div class="invoice-title">
                                            
                                            <h3 class="m-t-0">
                                            <!-- <img src="http://localhost/uniger/application/views/assets/images/logo-dark.png" alt="logo" height="32">                                            </h3> -->
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-6">
                                                <address>
                                                    <strong>Inquilino:</strong><br>
                                                    <?php echo $recibos->Usu_Nombres." ".$recibos->Usu_Apellidos; ?><br>
                                                    <strong>Apartamento:</strong><br>
                                                    <?php echo $recibos->Inm_Id; ?><br>
                                                    
                                                    Torres de la GIralda, Medellin
                                                </address>
                                            </div>
                                            <div class="col-6 text-right">
                                                <address>
                                                  <strong>Fecha:</strong><br>
                                                  <?php echo $recibos->Rec_Pag_Fecha_Expedicion; ?><br><br>
                                                </address>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="panel panel-default">
                                            <div class="p-2">
                                                <h3 class="panel-title font-20"><strong>Abonos</strong></h3>
                                            </div>
                                            <div class="">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <td><strong>Detalles</strong></td>
                                                            <td class="text-center"><strong>Precio</strong></td>
                                                            <td class="text-center"><strong>Cantidad</strong>
                                                            </td>
                                                            <td class="text-right"><strong>Total</strong></td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                        <tr>
                                                            <td>Administracion</td>
                                                            <td class="text-center">$ <?php echo $recibos->Rec_Pag_Administracion; ?></td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-right">$ <?php echo $recibos->Rec_Pag_Administracion; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Salon social</td>
                                                            <td class="text-center">$ <?php echo $recibos->Rec_Pag_Salon_Valor; ?></td>
                                                            <td class="text-center"><?php echo $recibos->Rec_Pag_Salon_cant; ?></td>
                                                            <td class="text-right">$ <?php echo $recibos->Rec_Pag_Salon_Valor + $recibos->Rec_Pag_Salon_cant ; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Multas</td>
                                                            <td class="text-center">$ <?php echo $recibos->Mul_Valor; ?></td>
                                                            <td class="text-center"><?php echo $recibos->Rec_Pag_Mul_Cant; ?></td>
                                                            <td class="text-right">$ <?php echo $recibos->Mul_Valor + $recibos->Rec_Pag_Mul_Cant; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="thick-line"></td>
                                                            <td class="thick-line"></td>
                                                            <td class="thick-line text-center">
                                                                <strong>Subtotal</strong></td>
                                                            <td class="thick-line text-right">$ <?php echo $recibos->Rec_Pag_Valor; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <td class="no-line"></td>
                                                            <td class="no-line"></td>
                                                            <td class="no-line text-center">
                                                                <strong>Total</strong></td>
                                                            <td class="no-line text-right"><h4 class="m-0">$ <?php echo $recibos->Rec_Pag_Valor; ?></h4></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="d-print-none">
                                                    <div class="pull-right">
                                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light"><i class="fa fa-print"></i></a>
                                                        <a href="<?php echo base_url('Recibos/pdf/'. $recibos->Rec_Pag_Id) ?>" class="btn btn-primary waves-effect waves-light "><i class="mdi mdi-download"></i> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div> <!-- end row -->

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
