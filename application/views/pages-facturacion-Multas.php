<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">

    <!-- container -->
    <div class="container-fluid">
        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="<?php echo base_url("/inicio");?>">Inicio</a></li>
               <li class="breadcrumb-item"><a href="<?php echo base_url("/recibos");?>">Recibos de pago</a></li>
               <li class="breadcrumb-item active">Multas</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Multas</h4>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <br>
                    <div class="card m-b-30">
                        <div class="card-header">
                                <h3>Multas</h3>
                        </div>
                        <div class="card-body">
                            <?php echo form_open('Recibos/multas');?>
                            <div class="form-group">

                                <label><b>Inmueble *</label>
                                <select class="form-control" name="Inmueble" id="Inmueble">
                                  <option value="">Seleccione</option>
                                    <?php foreach($recibos as $recibo){ ?>
                                    <option value="<?php echo $recibo->Inm_Id;?>"><?php echo $recibo->Inm_Id;?></option>
                                    <?php } ?>
                                </select>
                                <?php echo form_error('Inmueble'); ?>

                            </div>
                            <div class="form-group">
                                  <label>Valor *</label>
                                <input type="number" min="0"  placeholder="0.000" class="form-control" name="Valor">
                                <?php echo form_error('Valor'); ?>
                              </div>
                            <div class="form-group">
                                <label >Descripción *</label>

                                <textarea class="form-control" id="Descripcion" rows="3" name="Multa_Descripcion" value= "<?php  echo  set_value ( 'Multa_Descripcion' );  ?>"></textarea>
                                <?php echo form_error('Multa_Descripcion'); ?>
                            </div>
                            <button type="submit" class="btn btn-primary" onclick="form.submit();this.disabled=true">Registrar</button>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <?php $this->load->view('footer'); ?>
