<?php $this->load->view('header') ;?>
    <!-- wrapper -->
    <div class="wrapper">
        <!-- Contenedor -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="#">Inicio</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Usuarios</a>
                                </li>
                                <li class="breadcrumb-item active">Peticiones de Ingreso</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Peticiones de Ingreso</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            <!-- Contenido Principal -->
            <div class="row">
                <!-- Selector de menú -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show" data-toggle="tab" href="#home" role="tab" aria-selected="true">Peticiones Pendientes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-selected="false">Historial</a>
                    </li>
                </ul>
                <!-- Fin Selector Menú -->
            </div>
            <!-- Contenido -->
            <div class="tab-content">
                <div class="tab-pane p-3 active show" id="home" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if(!$usuarios){ ?>
                                <h3>No hay usuarios pendientes por aprobación</h3>
                            <?php }else{ ?>
                            
                            <!-- Tabla -->
                            <table class="table table-striped">
                                <!-- Encabezado -->
                                <thead>
                                    <tr>
                                        <th>Nombres</th>
                                        <th>Apto</th>
                                        <th>Comentarios</th>
                                        <th>Fecha Registro</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <!-- Fin Encabezado -->
                                <!-- Cuerpo -->
                                <tbody>
                                    <?php foreach($usuarios as $usuario){ ?>
                                        <?php 
                                            $slice = explode("$-$", $usuario->Usu_Comentario);
                                        ?>
                                    <tr>
                                        <th scope="row"><?php echo $usuario->Usu_Nombres. " " .$usuario->Usu_Apellidos; ?></th>
                                        <td style="display: none"><?php echo $usuario->Usu_Id; ?></td>
                                        <td><?php echo $usuario->Usu_Apto; ?></td>
                                        <td><?php echo $usuario->Usu_Comentario; ?></td>
                                        <td><?php echo $usuario->Usu_Fecha_Registro; ?></td>
                                        <td>
                                        <a href="detallesNuevo/<?php echo $usuario->Usu_Id; ?>" class="btn btn-outline-info waves-effect waves-light">
                                                <i class="mdi mdi-eye"></i>
                                                </a>
                                            <a href="" class="btn btn-outline-success waves-effect waves-light mr-2" data-toggle="modal" data-target=".modalAprobado">
                                                <i class="mdi mdi-check"></i>
                                            </a>
                                            <a href="" class="btn btn-outline-danger waves-effect waves-light" data-toggle="modal" data-target=".modalRechazado">
                                                <i class="mdi mdi-close"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <!-- Modal Detalles -->
                                    <div class="modal fade modalDetalle" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0" id="myLargeModalLabel"><?php echo $usuario->Usu_Nombres. " " .$usuario->Usu_Apellidos; ?></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <p><strong>Apto:</strong></p>
                                                            <p><strong>Comentarios:</strong></p>
                                                            <p><strong>Fecha Registro:</strong></p>
                                                            <p><strong>Estado:</strong></p>
                                                            <p><strong>Email:</strong></p>
                                                            <p><strong>Teléfono:</strong></p>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <p><?php echo $usuario->Usu_Apto; ?></p>
                                                            <p><?php echo $usuario->Usu_Comentario; ?></p>
                                                            <p><?php echo $usuario->Usu_Fecha_Registro; ?></p>
                                                            <p>Pendiente</p>
                                                            <p><?php echo $usuario->Usu_Email; ?></p>
                                                            <p><?php echo $usuario->Usu_Telefono_Celular; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal detalle -->
                                    <!-- Modal Aprobado -->
                                    <div class="modal fade modalAprobado" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Aprobar petición de: <?php echo $usuario->Usu_Nombres. " " .$usuario->Usu_Apellidos; ?></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            x    </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <?php echo form_open('Usuarios/aprobar'); ?>
                                                            
                                                            <div class="m-t-20">
                                                                <h6 class="text-muted"><b>Rol General</b></h6>
                                                                <p class="text-muted m-b-15 font-13">
                                                                    Rol dentro de la aplicación
                                                                </p>
                                                                <select name="Rol_General" id="" class="form-control" required="">
                                                                    <option value="">Seleccione...</option>
                                                                    <option value="0">Administrador</option>
                                                                    <option value="1">usuario</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="m-t-20">
                                                                <h6 class="text-muted"><b>Comentarios</b></h6>
                                                                <p class="text-muted m-b-15 font-13">
                                                                    Estos comentarios se enviarán al usuario en un email.
                                                                </p>
                                                                <input type="hidden" name="U_refAprobado" value="<?php echo $this->encryption->encrypt($usuario->Usu_Id); ?>">
                                                                <textarea name="comentariosAprobado" class="form-control" maxlength="225" rows="3" placeholder="Este campo tiene un límite de 255 caracteres."></textarea>
                                                            </div>
                                                            <div class="m-t-20 d-flex justify-content-center">
                                                                <button type="submit" class="btn btn-success waves-effect waves-light">Finalizar</button>
                                                            </div>
                                                            <?php echo form_close(); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal detalle -->
                                    <!-- Modal Rechazado -->
                                    <div class="modal fade modalRechazado" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title mt-0" id="myLargeModalLabel">Rechazar petición de: <?php echo $usuario->Usu_Nombres. " " .$usuario->Usu_Apellidos; ?></h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <?php echo form_open('Usuarios/rechazar'); ?>
                                                            <div class="m-t-20">
                                                                <h6 class="text-muted"><b>Comentarios</b></h6>
                                                                <p class="text-muted m-b-15 font-13">
                                                                    Estos comentarios se enviarán al usuario en un email.
                                                                </p>
                                                                <input type="hidden" name="U_refRechazado" value="<?php echo $this->encryption->encrypt($usuario->Usu_Id); ?>">
                                                                <textarea id="textarea" class="form-control" name="comentariosRechazado" maxlength="225" rows="3" placeholder="Este campo tiene un límite de 255 caracteres."></textarea>
                                                            </div>
                                                            <div class="m-t-20 d-flex justify-content-center">
                                                                <button type="submit" class="btn btn-danger waves-effect waves-light">Finalizar</button>
                                                            </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal rechazado -->
                                    <?php } ?>
                                </tbody>
                                <!-- Fin Cuerpo -->
                            </table>
                            <!-- Fin Tabla -->
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane p-3" id="profile" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Tabla -->      
                            <table class="table table-striped" id="datatable">
                                <!-- Encabezado -->
                                <thead>
                                    <tr>
                                        <th>Nombres</th>
                                        <th>Telefono</th>
                                        <th>Correo</th>
                                        <th>Estado</th>
                                        <th>Detalles</th>
                                    </tr>
                                </thead>
                                <!-- Fin Encabezado -->
                                <!-- Cuerpo -->
                                <tbody>
                                <?php if(!empty($historial)){ ?>
                                <?php foreach($historial as $historia)
                                        {?>
                                        <tr>
                                            <th scope="row"><?php echo $historia->Reg_Nombres." ".$historia->Reg_Apellidos; ?></th>
                                            <td><?php echo $historia->Reg_Telefono; ?></td>
                                            <td><?php echo $historia->Reg_Email; ?></td>
                                            <td><?php echo $historia->Reg_Estado; ?></td>
                                            <td>
                                            <a href="historialUsuarios/<?php echo $historia->Reg_Email; ?>" class="btn btn-outline-info waves-effect waves-light">
                                                <i class="mdi mdi-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                <!-- Modal Historial -->
                                <div class="modal fade modalHistorial" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0" id="myLargeModalLabel"><?php echo $historia->Reg_Nombres. " " .$historia->Reg_Apellidos; ?></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <p><strong>Apto:</strong></p>
                                                            <p><strong>Comentarios:</strong></p>
                                                            <p><strong>Fecha Registro:</strong></p>
                                                            <p><strong>Estado:</strong></p>
                                                            <p><strong>Email:</strong></p>
                                                            <p><strong>Teléfono:</strong></p>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <p><?php echo $historia->Reg_Apto; ?></p>
                                                            <p><?php echo $historia->Reg_Comentario; ?></p>
                                                            <p><?php echo $historia->Reg_Fecha_Registro; ?></p>
                                                            <p><?php echo $historia->Reg_Estado; ?></p>
                                                            <p><?php echo $historia->Reg_Email; ?></p>
                                                            <p><?php echo $historia->Reg_Telefono; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal detalle -->
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal rechazado -->
                                </tbody>
                                <!-- Fin Cuerpo -->
                            </table>
                            <!-- Fin Tabla -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- fin container -->
    </div>
    <!-- Fin wrapper -->
<script>
    var base_url = "<?php echo base_url();?>";
</script>
        <!-- Required datatable js -->
        <?php $this->load->view('footer'); ?>
        <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?php echo base_url('application/views/');?>assets/pages/datatables.init.js"></script>
<?php if ($this->session->flashdata('result')) {?>
    <script>
        $(document).ready(function(){
            alertify.success("Proceso realizado con éxito");
        });
    </script>   
<?php }; ?> 