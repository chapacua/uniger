<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="#">Inicio</a>
                                </li>
                                <li class="breadcrumb-item active">Usuarios</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Usuarios</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            <div class="row">
                <!-- Item -->
                <div class="col-lg-3 offset-md-1 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                            <a href="<?php echo base_url('usuarios/peticiones'); ?>">
                                <div class="card-icon">
                                    <i class="mdi mdi-file"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Peticiones de registro</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- Item -->
                <div class="col-lg-4 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                            <a href="<?php echo base_url('usuarios/listar'); ?>">
                                <div class="card-icon">
                                    <i class="mdi mdi-account-settings-variant"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Administración de Usuarios</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- Item -->
                <div class="col-lg-3 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                            <a href="<?php echo base_url('usuarios/reportes'); ?>">
                                <div class="card-icon">
                                    <i class="mdi mdi-chart-bar"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Reportes</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Fin container -->
    </div>
    <!-- Fin wrapper -->

<?php $this->load->view('footer'); ?>