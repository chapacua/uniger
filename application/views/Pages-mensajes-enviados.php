<?php $this->load->view('header'); ?>

<!-- wrapper -->
<div class="wrapper">
    <!-- container -->
    <div class="container-fluid">

        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="pages-inicio-comunicaciones.html">Comunicaciones</a>
                            </li>
                            <li class="breadcrumb-item active">
                                <a href="pages-mensajes.html">Mensajes</a>
                            </li>
                        </ol>
                    </div>
                    <h4 class="page-title">Mensajes</h4>
                </div>
            </div>
        </div>
        <!-- Fin titulo pagina y miga de pan -->
        <!--Barra-->
        <div class="row">
            <div class="col-lg-2">
                <div class="card m-b-30">
                    <div class="card-body">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link active" href="<? echo base_url('/Mensajes/Redactar_mensaje');?>">Redactar</a>
                            </li>
                        </ul>
                        <ul class="nav flex-column">
                            <li class="nav-item">
                            <a class="nav-link active" href="<? echo base_url('/Mensajes/listar_Recibidos');?>">Recibidos</a>
                            </li>
                        </ul>
                        <ul class="nav flex-column">
                            <li class="nav-item">
                            <a class="nav-link active" href="<? echo base_url('/Mensajes/Mensajes_Enviados');?>">Enviados</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!--Mensajes-->
            <div class="col-lg-10">
                <div class="card m-b-30">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Enviados</h4>


                        <table class="table table-hover">
                            <div class="tab-pane p-3" id="profile" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Tabla -->
                                                    <!-- Tabla -->      
                            <table class="table table-striped" id="datatable">
                                <!-- Encabezado -->
                                <thead>
                                    <tr>
                                        <th>Asunto</th>
                                        <th>Correo</th>
                                        <th>Detalles</th>
                                    </tr>
                                </thead>
                                <!-- Fin Encabezado -->
                                <!-- Cuerpo -->
                                <tbody>
                              
                                         <tr>
                                            <th> </th>
                                            <td> </td>
                                            <td>
                                            <a href="" class="btn btn-outline-info waves-effect waves-light">
                                                <i class="mdi mdi-eye"></i>
                                            </a>
                                            <a href="" class="btn btn-outline-danger waves-effect waves-light mr-2">
                                                            <i class=" mdi mdi-delete"></i>
                                            </a>
                                            </td>
                                        </tr>
                                </tbody>
                                <!-- Fin Cuerpo -->
                            </table>
                            <!-- Fin Tabla -->

                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- Fin container -->
                </div>
                <!-- Fin wrapper -->

                <?php $this->load->view('footer'); ?>