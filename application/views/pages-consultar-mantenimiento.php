<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- Contenedor -->
    <div class="container-fluid">

        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Mantenimiento</a>
                            </li>
                            <li class="breadcrumb-item active">Consultar</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Consultar mantenimiento</h4>
                </div>
            </div>
        </div>

                    <div class="row">
                        <!-- Tabla -->
                        <table class="table table-striped">
                            <!-- Encabezado -->

                            <thead>

                                <tr>
                                    <div class="card-header">
                                        <form>
                                            <h6>
                                                <i class="dripicons-view-list"></i>
                                                &nbsp; Resultados

                                            </h6>
                                        </form>
                                    </div>
                                    <th>Tipo de Mantenimiento</th>
                                    <th>Fecha</th>
                                    <th>Duración</th>
                                    <th>Comentario inicial</th>
                                    <th>Comentario final</th>
                                    <th>Calificación</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <!-- Fin Encabezado -->
                            <!-- Cuerpo -->
                            <tbody>
                                <?php foreach($mantenimiento as $mante){?>
                                <tr>
                                    <td>
                                        <?php echo $mante->Man_Tipo?> </td>
                                    <td>
                                        <?php echo $mante->Man_Fecha?> </td>
                                    <td>
                                        <?php echo $mante->Man_Tiempo." Horas"?> </td>
                                    <td>
                                        <?php echo $mante->Man_Comentario_Inicial?> </td>
                                    <td>
                                        <?php echo $result = (!is_null($mante->Man_Comentario_Final)) ? $mante->Man_Comentario_Final : "Sin revisar" ;?> </td>
                                    <td>
                                        <?php echo $result =  ($mante->Man_Calificacion > 0) ? $mante->Man_Calificacion : "Sin revisar" ;?> </td>
                                <td>
                                        <a href="#" class="btn btn-outline-danger waves-effect waves-light mr-2" onclick="LanzarModal(<?php echo $mante->Man_Id; ?>);">
                                            <i class=" mdi mdi-delete"></i>
                                        </a>
                                        <a href="<?php echo base_url("Mantenimiento/editar_mantenimiento/".$mante->Man_Id);?>" class="btn btn-outline-info waves-effect waves-light mr-2">
                                            <i class="mdi mdi-check"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <!-- Fin Cuerpo -->
                        </table>
                        <!-- Fin Tabla -->
                    </div>
                </div>
                <!-- fin container -->

    <!-- MODAL DELETE -->
    <div class="modal fade" id="ModalDeleteMantenimiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-1 col-lg-1 col-md-1"></div>
                        <div class="col-xl-10 col-lg-10 col-md-10">
                            <div class="form-group">
                                <p>¿Está seguro que desea eliminar el mantenimiento?</p>
                            </div>
                            
                            <input type="text" class="form-control" name="TokenDel" id="TokenDel" hidden>
                                
                        </div>
                        <div class="col-xl-1 col-lg-1 col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="EliminarMantenimiento();">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Fin wrapper -->
<?php $this->load->view('footer'); ?>

<script type="text/javascript">

function LanzarModal(ID){

    document.getElementById("TokenDel").value = ID;
    $('#ModalDeleteMantenimiento').modal('show');
}


function EliminarMantenimiento(){

    var TokenDel = document.getElementById("TokenDel");

    var Registro = {
        "Res_Token": TokenDel.value
    };

    $.ajax({
        url: '<? echo base_url('Mantenimiento/borrar_mantenimiento');?>',
        method: 'POST',
        data: Registro,
        success: function(response)
        {
            
            $('#ModalDeleteMantenimiento').modal('hide');
            window.location = '/uniger/mantenimiento/Listar/delete/';

        },
        error: function (err) {
            alert("Ocurrió un error: " + JSON.stringify(err, null, 2));
        }
    });
    }

</script>

<?php 
    if ($this->uri->segment(3)== "success")
    {?>
        <script>
            $(document).ready(function () {
                alertify.success("Mantenimiento creado correctamente");
            });
        </script>

<?php } else if ($this->uri->segment(3)== "delete") 
    {?>
        <script>
            $(document).ready(function () {
                alertify.success("Mantenimiento borrado correctamente");
            });
        </script>
<?php } else if ($this->uri->segment(3)== "update")
    {?>
        <script>
            $(document).ready(function () {
                alertify.success("Mantenimiento calificado correctamente");
            });
        </script>
<?php } ?>