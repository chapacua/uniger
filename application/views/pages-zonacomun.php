<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- container -->
    <div class="container-fluid">

        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item active">Zonas Comunes</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Zonas Comunes</h4>
                </div>
            </div>
        </div>
        <!-- Fin titulo pagina y miga de pan -->
        <div class="row">
        <?php if ($this->session->userdata('Usu_Rol') == 0) { ?>
            <!-- Item -->
            <div class="col-lg-3 center-align">
                <div class="card m-b-30 text-white bg-gray">
                    <div class="card-body">
                        <a href= "<? echo base_url('/zonacomun/crear');?>">
                            <div class="card-icon">
                                <i class="ion-android-friends"></i>
                            </div>
                            <div class="card-text">
                                <p class="card-text">Registrar Zona Común</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Item -->
            <div class="col-lg-3 center-align">
                <div class="card m-b-30 text-white bg-gray">
                    <div class="card-body">
                        <a href="<? echo base_url('/zonacomun/listar');?>">
                            <div class="card-icon">
                                <i class="mdi mdi-magnify"></i>
                            </div>
                            <div class="card-text">
                                <p class="card-text">Consultar Zona Común</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Item -->
            <div class="col-lg-3 center-align">
                <div class="card m-b-30 text-white bg-gray">
                    <div class="card-body">
                        <a href="<? echo base_url('/zonacomun/Reportes');?>">
                            <div class="card-icon">
                                <i class="mdi mdi-chart-bar"></i>
                            </div>
                            <div class="card-text">
                                <p class="card-text">Reportes</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Item -->
            <div class="col-lg-3 center-align">
                <div class="card m-b-30 text-white bg-gray">
                    <div class="card-body">
                    <a href= "<? echo base_url('/proveedores/crear');?>">
                            <div class="card-icon">
                                <i class="mdi mdi-cube-send"></i>
                            </div>
                            <div class="card-text">
                                <p class="card-text">Proveedores</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
            <!-- Item -->
            <div class="row">
                    <div class="col-lg-3 center-align">
                            <div class="card m-b-30 text-white bg-gray">
                                <div class="card-body">
                                    <a href="<? echo base_url('/proveedores/listar');?>">
                                        <div class="card-icon">
                                            <i class="mdi mdi-magnify"></i>
                                        </div>
                                        <div class="card-text">
                                            <p class="card-text">Consultar Proveedores</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Item -->
                    <div class="col-lg-3 center-align">
                        <div class="card m-b-30 text-white bg-gray">
                            <div class="card-body">
                                <a href= "<? echo base_url('/mantenimiento/crear');?>">
                                <div class="card-icon">
                                    <i class="ion-settings"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Mantenimiento</p>
                                </div>
                            </a>
                        </div>
                </div>
            </div>
            <!-- Item -->
                    <div class="col-lg-3 center-align">
                        <div class="card m-b-30 text-white bg-gray">
                            <div class="card-body">
                                <a href="<? echo base_url('/mantenimiento/listar');?>">
                                    <div class="card-icon">
                                        <i class="mdi mdi-magnify"></i>
                                    </div>
                                    <div class="card-text">
                                        <p class="card-text">Consultar Mantenimiento</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
            <!-- Item -->
                <div class="col-lg-3 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                            <a href="<? echo base_url('reservas');?>">
                                <div class="card-icon">
                                    <i class="mdi mdi-calendar-multiple"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Reservas</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
        </div>


                 </div>
            </div>
         </div>
    </div>
   
</div>

<br>
<?php $this->load->view('footer'); ?>