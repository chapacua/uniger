<?php $this->load->view('header') ;?>
    <!-- wrapper -->
    <div class="wrapper">
        <!-- Contenedor -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="#">Inicio</a>
                                </li>
                                <li class="breadcrumb-item active">Mapa de navegación</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Mapa de navegación</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            <!-- Contenido Principal -->

            <div class="row">

                <div class="col-lg-6">
                    <div class="card m-b-30">
                        <div class="card-body">

                            <h4 class="mt-0 header-title">Recibos de pago</h4>
                            <p class="text-muted m-b-30 font-14">Mapa de navegación recibos de pago.</p>

                            <div class="custom-dd dd" id="nestable_list_1">
                                <ol class="dd-list">
                                    <li class="dd-item">
                                        <div class="dd-handle">
                                            <a href="<?php echo base_url('recibos/consultar'); ?>">Recibos de pago</a>
                                        </div>
                                        <ol class="dd-list">
                                            <li class="dd-item" data-id="4">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('recibos/consultar'); ?>">Consultar multa</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('recibos/reportes'); ?>">Reportes multas</a>
                                                </div>
                                            </li>
                                        </ol>
                                    </li>
                                    <li class="dd-item" data-id="2">
                                        <div class="dd-handle">
                                            <a href="<?php echo base_url('multas'); ?>">Multas</a>
                                        </div>
                                        <ol class="dd-list">
                                            <li class="dd-item" data-id="3">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('multas/crear'); ?>">Crear multa</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="4">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('multas/consultar'); ?>">Consultar multa</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('multas/reportes'); ?>">Reportes multas</a>
                                                </div>
                                            </li>
                                        </ol>
                                    </li>
                                    <li class="dd-item" data-id="2">
                                        <div class="dd-handle">
                                            <a href="<?php echo base_url('cuotas_extraordinarias'); ?>">Presupuestos</a>
                                        </div>
                                        <ol class="dd-list">
                                            <li class="dd-item" data-id="3">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('cuotas_extraordinarias/crear'); ?>">Crear Presupuesto</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="4">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('cuotas_extraordinarias/consultar'); ?>">Consultar Presupuesto</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('cuotas_extraordinarias/reportes'); ?>">Reportes Presupuestos</a>
                                                </div>
                                            </li>
                                        </ol>
                                    </li>
                                    <li class="dd-item" data-id="2">
                                        <div class="dd-handle">
                                            <a href="<?php echo base_url('presupuestos'); ?>">Cuota extraordinaria</a>
                                        </div>
                                        <ol class="dd-list">
                                            <li class="dd-item" data-id="3">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('presupuestos/crear'); ?>">Crear cuota extraordinaria</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="4">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('presupuestos/consultar'); ?>">Consultar cuota extraordinaria</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('presupuestos/reportes'); ?>">Reportes cuotas extraordinarias</a>
                                                </div>
                                            </li>
                                        </ol>
                                    </li>
                                    <li class="dd-item" data-id="2">
                                        <div class="dd-handle">
                                            <a href="<?php echo base_url('abonos'); ?>">Abonos</a>
                                        </div>
                                        <ol class="dd-list">
                                            <li class="dd-item" data-id="3">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('abonos/crear'); ?>">Crear abono</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="4">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('abonos/consultar'); ?>">Consultar abono</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('abonos/reportes'); ?>">Reportes abonos</a>
                                                </div>
                                            </li>
                                        </ol>
                                    </li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end col -->

                <div class="col-lg-6">
                    <div class="card m-b-30">
                        <div class="card-body">

                            <h4 class="mt-0 header-title">Zonas comunes</h4>
                            <p class="text-muted m-b-30 font-14">Mapa de navegación zonas comunes.</p>

                            <div class="custom-dd dd" id="nestable_list_1">
                                <ol class="dd-list">
                                    <li class="dd-item">
                                        <div class="dd-handle">
                                            <a href="<?php echo base_url('zonacomun/index'); ?>">Zonas comunes</a>
                                        </div>
                                        <ol class="dd-list">
                                            <li class="dd-item" data-id="4">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('zonacomun/crear'); ?>">Registrar zona común</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('zonacomun/listar'); ?>">Consultar zona común</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('zonacomun/reportes'); ?>">Reportes</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('proveedores/crear'); ?>">Proveedores</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('proveedores/listar'); ?>">Consultar Proveedores</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('mantenimiento/crear'); ?>">Mantenimiento</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('mantenimiento/listar'); ?>">Consultar mantenimiento</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('reservas'); ?>">Reservas</a>
                                                </div>
                                            </li>
                                        </ol>
                                    </li>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-6">
                    <div class="card m-b-30">
                        <div class="card-body">

                            <h4 class="mt-0 header-title">Inmuebles</h4>
                            <p class="text-muted m-b-30 font-14">Mapa de navegación de inmuebles.</p>

                            <div class="custom-dd dd" id="nestable_list_1">
                                <ol class="dd-list">
                                    <li class="dd-item">
                                        <div class="dd-handle">
                                            <a href="<?php echo base_url('Inmuebles/crear_inmueble'); ?>">Inmuebles</a>
                                        </div>
                                        <ol class="dd-list">
                                            <li class="dd-item" data-id="4">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('Inmuebles/listar'); ?>">Consultar inmueble</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('Inmuebles/Reportes'); ?>">Reportes inmuebles</a>
                                                </div>
                                            </li>
                                        </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end col -->

                <div class="col-lg-6">
                    <div class="card m-b-30">
                        <div class="card-body">

                            <h4 class="mt-0 header-title">Usuarios</h4>
                            <p class="text-muted m-b-30 font-14">Mapa de navegación usuarios.</p>

                            <div class="custom-dd dd" id="nestable_list_1">
                                <ol class="dd-list">
                                    <li class="dd-item">
                                        <div class="dd-handle">
                                            <a href="<?php echo base_url('usuarios'); ?>">Usuarios</a>
                                        </div>
                                        <ol class="dd-list">
                                            <li class="dd-item" data-id="4">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('usuarios/listar'); ?>">Administración de usuarios</a>
                                                </div>
                                            </li>
                                            <li class="dd-item" data-id="5">
                                                <div class="dd-handle">
                                                    <a href="<?php echo base_url('usuarios/reportes'); ?>">Reportes</a>
                                                </div>
                                            </li>
                                            <li class="dd-item">
                                            <div class="dd-handle">
                                                <a href="<?php echo base_url('usuarios/peticiones'); ?>">Peticiones de registro</a>
                                            </div>
                                        </ol>
                                    </li>
                            </div>
                        </div>
                    </div>
        <!-- fin container -->
    </div>
    <!-- Fin wrapper -->
    <script>
        var base_url = "<?php echo base_url();?>";
    </script>
    <!-- Required datatable js -->
    <?php $this->load->view('footer'); ?>
        <?php if ($this->session->flashdata('result')) {?>
            <script>
                $(document).ready(function() {
                    alertify.success("Proceso realizado con éxito");
                });
            </script>
            <?php }; ?>