<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- container -->
    <div class="container-fluid">

        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                            <a href="<?php echo base_url('Inmuebles'); ?>">Inmuebles</a>                           
                            </li>
                        </ol>
                    </div>
                    <h4 class="page-title">Registrar inmueble</h4>
                </div>
            </div>
        </div>
                <!-- Fin titulo y mida de pan -->
        <div class="card m-b-30">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="p-20">
                            <?php echo form_open('Inmuebles/crear_inmueble');?>
                            <div class="form-group">
                                <label>Tipo de inmueble *</label>
                                <select class="form-control" name="Inm_Tipo">
                                    <option value="Apartamento">Apartamento</option>
                                    <option value="Cuarto Util">Cuarto Util</option>
                                    <option value="Parqueadero">Parqueadero</option>
                                    <option value="Apartaestudio">Apartaestudio</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Dirección</label>
                                <input type="text" value="Torres de la giralda" class="form-control" name="Inm_Direccion" disabled>
                            </div>
                            <div class="form-group">
                                <label>Propietario *</label>
                                <select class="selectpicker col-12 d-block" data-live-search="true" name="Usu_Id" required>
                                    <option value="">Seleccione</option>
                                    <?php foreach($usuarios as $usuario): ?>
                                    <option data-tokens="<?php echo $usuario->Usu_Nombres." ".$usuario->Usu_Apellidos;?>" value="<?php echo $usuario->Usu_Id;?>">
                                        <?php echo $usuario->Usu_Nombres." ".$usuario->Usu_Apellidos;?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>


                            </div>
                            <div class="form-group">
                                <label>Número de inmueble *</label>
                                <input type="text" placeholder="" id="Inm_Id" class="form-control" name="Inm_Id" required>
                                <?php echo form_error('Inm_Id'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="p-20">
                            <div class="form-group">
                                <label for="Inm_Numero_Matricula">Número de matrícula *</label>
                                <input class="form-control" name="Inm_Numero_Matricula" id="Inm_Numero_Matricula" required>
                                <?php echo form_error('Inm_Numero_Matricula'); ?>
                            </div>
                            <div class="form-group">
                                <label>Área construida *</label>
                                <input type="text" placeholder="" class="form-control" name="Inm_Area_Construida" value="<?php  echo  set_value ( 'Inm_Area_Construida' );  ?>">
                                <?php echo form_error('Inm_Area_Construida'); ?>
                            </div>
                            <div class="form-group">
                                <label>Área privada *</label>
                                <input type="text" placeholder="" class="form-control" name="Inm_Area_Privada" value="<?php  echo  set_value ( 'Inm_Area_Privada' );  ?>">
                                <?php echo form_error('Inm_Area_Privada'); ?>
                            </div>
                            <div class="form-group">
                                <label>Coeficiente de propiedad *</label>
                                <input type="text" placeholder="" class="form-control" name="Inm_Coeficiente_Propiedad" value="<?php  echo  set_value ( 'Inm_Coeficiente_Propiedad' );  ?>">
                                <?php echo form_error('Inm_Coeficiente_Propiedad'); ?>
                            </div>
                        </div>
                    </div>
                    <div style="left: 200px; margin: 0 auto;">
                        <button type="submit" class="btn btn-primary">Registrar</button>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>
        </div>

        <br>
        <?php $this->load->view('footer'); ?>
