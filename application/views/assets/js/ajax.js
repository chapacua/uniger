/*
 Content: Funciones ajax
 Author: Alejandro Velasco
 File: ajax.js
 */

//Formato para campos que contengasn valores de dinero
// new cleave('.input-val', {
//     numeral: true,
//     numeraldecimalmark: ',',
//     delimiter: '.'
// });
$(document).ready(function() {
    $('.input-val').keyup(function(event) {

        // skip for arrow keys
        if(event.which >= 37 && event.which <= 40) return;
      
        // format number
        $(this).val(function(index, value) {
          return value
          .replace(/\D/g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
          ;
        });
      });

    $("#help").click(function()
    {
        introJs()
        .setOption("nextLabel", " Siguiente")
        .setOption("skipLabel", " Saltar")
        .setOption("prevLabel", " Atrás")
        .start();
    });
    //Ajax para validar el número del apto en el login
    $("#regAptoInput").blur(function() {
        var id = $("#regAptoInput").val();
        $.ajax({
            url: base_url + "Validation/validarInmueblePorId/" + id,
            type: "GET",
            beforeSend: function() {
                $("#validando").remove(),
                    $("#regAptoInput").after(
                        '<div class="form-control-feedback" id="validando">Validando...</div>'
                    );
            },
            success: function(data) {
                if (data == 2) {
                    $("#regAptoInput").addClass("form-control-warning parsley-error");
                    $("#validando").remove(),
                        $("#regAptoInput").after(
                            '<div class="text-danger" id="validando">Apartamento inválido.</div>'
                        );
                } else {
                    $("#regAptoInput").removeClass("form-control-warning parsley-error");
                    $("#validando").remove();
                }
            }
        });
    }); //End Ajax

    
    $("#apartamento").on("change", function() {
        var id = $("#apartamento").val();
        var data = {
            id: id
        };
        $.ajax({
            url: "http://localhost/uniger/Recibos/cargar_Select/",
            type: "POST",
            data: data,
            dataType: "json",
            success: function(data) {
                $("#Multas").val(data);
            }
        });
    });

    $(".modalHistorial").click(function() {
        var email = $(this).attr("data-email");
        alert('asd');
    });

    function createTable(data, headers) {
        var tbl = '<table class="table table-striped" id="mytable"></table>';
        $("#showData").append(tbl);
        $("#mytable").append("<thead>");
        $("#mytable").append("<tr>");
        for (var index = 0; index < headers.length; index++) {
            var td = "<th><strong>" + headers[index] + "</strong></th>";
            $("#mytable").append(td);
        }
        $("#mytable").append("<tr>");
        $("#mytable").append("</thead>");

        for (var i = 0; i < data.length; i++) {
            var tr = "<tr>";
            var td1 =
                "<td>" +
                data[i]["Reg_Nombres"] +
                " " +
                data[i]["Reg_Apellidos"] +
                "</td>";
            var td2 = "<td>" + data[i]["Reg_Telefono"] + "</td>";
            var td3 = "<td>" + data[i]["Reg_Email"] + "</td>";
            var td4 = "<td>" + data[i]["Reg_Comentario"] + "</td>";
            var td5 = "<td>" + data[i]["Reg_Estado"] + "</td>";
            var td6 = "<td>" + data[i]["Reg_Fecha_Registro"] + "</td>";
            var trc = "</tr>";
            $("#mytable").append(tr + td1 + td2 + td3 + td4 + td5 + td6 + trc);
        }
    }

    $("#form-update").on("submit", function(e) {
        e.preventDefault();

        console.log($(this).serialize());
        $.ajax({
            url: $(this).attr("action"),
            type: $(this).attr("method"),
            data: $(this).serialize(),
            beforeSend: function() {},
            success: function(data) {
                alert(data.success);
                alert(data.error);
                if ($.isEmptyObject(data.error)) {
                    $(".print-error-msg").css("display", "none");
                } else {
                    $(".print-error-msg").css("display", "block");
                    $(".print-error-msg").html(data.error);
                }
            }
        });
    });

    //Confirmación de creación de presupuesto
    $("#conf_crear_presupuesto").submit(function(event) {
        event.preventDefault();
        var url = $(this).attr("action");
        var type = $(this).attr("method");
        var data = $(this).serialize();
        $.bsAlert.confirm(
            "Este proceso podría tomar algunos minutos. ¿Está seguro que desea crear este presupuesto?",
            function() {
                crearPresupuesto(url, type, data);
            }
        );
    });

    function crearPresupuesto(url, type, data) {
        $.ajax({
            url: url,
            type: type,
            data: data,
            beforeSend: function() {
                $("#submitBtn").addClass("d-none");
                $("#loader").removeClass("d-none");
            },
            success: function(response) {
                var bandera = false;
                $(".form-control").removeClass('is-invalid')
                .parents(".form-group")
                .find("#error")
                .html('');
                $.each(response, function(key, value) {
                    if (value != "") {
                        $("#input-" + key).addClass("is-invalid");
                        $("#input-" + key)
                            .parents(".form-group")
                            .find("#error")
                            .html(value);
                        bandera = true;
                    }
                });
                if (bandera == false) {
                    alertify.success("Presupuesto creado");
                }
            },
            complete: function() {
                $("#submitBtn").removeClass("d-none");
                $("#loader").addClass("d-none");
            }
        });
    }

    //Confirmación de eliminación de presupuesto
    $(".borrarPresupuesto").click(function(event) {
        event.preventDefault();
        var presupuestoId = $(this).attr("id");
        var split = presupuestoId.split("-");
        var id = split[1];
        $.bsAlert.confirm(
            "Este proceso podría tomar algunos minutos y eliminará todos los registros asociados a este presupuesto, incluyendo multas. ¿Está seguro que desea eliminar este presupuesto?",
            function() {
                eliminarPresupuesto(base_url, id)
            }
        );
    });

    function eliminarPresupuesto(url, id) {
        $.ajax({
            url: url + "presupuestos/eliminar",
            type: 'POST',
            data: {
                id: id
            },
            beforeSend: function() {
                $("#pre-" + id).addClass("d-none");
                $("#delete-" + id).removeClass("d-none");
            },
            success: function(response) {
                console.log(response);

            },
            complete: function() {
                alertify.success("Presupuesto eliminado");
                alertify.success("Detalles presupuesto eliminados");
                alertify.success("Cuotas extraordinarias eliminadas");
                alertify.success("Multas eliminadas");
                $("#pre-" + id).removeClass("d-none");
                $("#delete-" + id).addClass("d-none");
            }
        });
    }

    //Confirmación de creación de cuota extraordinaria
    $("#conf_crear_cuota_extraordinaria").submit(function(event) {
        event.preventDefault();
        var url = $(this).attr("action");
        var type = $(this).attr("method");
        var data = $(this).serialize();
        $.bsAlert.confirm(
            "Este proceso podría tomar algunos minutos. ¿Está seguro que desea crear esta cuota extraordinaria?",
            function() {
                crearCuotaExtraordinaria(url, type, data);
            }
        );
    });

    function crearCuotaExtraordinaria(url, type, data) {
        $.ajax({
            url: url,
            type: type,
            data: data,
            beforeSend: function() {
                $("#submitBtnCuota").addClass("d-none");
                $("#loaderCuota").removeClass("d-none");
            },
            success: function(response) {
                $(".form-control").removeClass('is-invalid')
                .parents(".form-group")
                .find("#error")
                .html('');
                $.each(response, function(key, value) {
                    if (value != "") {
                        $("#input-" + key).addClass("is-invalid");
                        $("#input-" + key)
                            .parents(".form-group")
                            .find("#error")
                            .html(value);
                    }
                });
                if (response == "1") {
                    window.location.replace(base_url);
                }
                console.log(response);
            },
            complete: function() {
                $("#submitBtnCuota").removeClass("d-none");
                $("#loaderCuota").addClass("d-none");
                /*$('#conf_crear_cuota_extraordinaria').trigger("reset");*/
            }
        });

    }

    $("input[type='radio']").on('change', function() {
        var selectedValue = $("input[name='input-usuario-registrado']:checked").val();
        if (selectedValue == "Si") {
            if ($('#loConoce').hasClass('d-none')) {
                $('#loConoce').removeClass('d-none');
            }
            if (!$('#noLoConoce').hasClass('d-none')) {
                $('#noLoConoce').addClass('d-none');
            }
        } else if (selectedValue == "No") {
            if ($('#noLoConoce').hasClass('d-none')) {
                $('#noLoConoce').removeClass('d-none');
            }
            if (!$('#loConoce').hasClass('d-none')) {
                $('#loConoce').addClass('d-none');
            }
        }
    });

    //Confirmación de creación de multas
    $("#conf_crear_multa").submit(function(event) {
        event.preventDefault();
        var url = $(this).attr("action");
        var type = $(this).attr("method");
        var data = $(this).serialize();
        $.bsAlert.confirm(
            "¿Está seguro que desea crear esta multa?",
            function() {
                crearMulta(url, type, data);
            }
        );
    });

    function crearMulta(url, type, data) {
        $.ajax({
            url: url,
            type: type,
            data: data,
            beforeSend: function() {
                $("#submitBtn").addClass("d-none");
                $("#loader").removeClass("d-none");
            },
            success: function(response) {
                var bandera = false;
                $(".form-control").removeClass('is-invalid')
                .parents(".form-group")
                .find("#error")
                .html('');
                $.each(response, function(key, value) {
                    if (value != "") {
                        $("#input-" + key).addClass("is-invalid");
                        $("#input-" + key)
                            .parents(".form-group")
                            .find("#error")
                            .html(value);
                        bandera = true;
                    }
                });
                if (bandera == false) {
                    alertify.success("Multa creada");
                }
            },
            complete: function() {
                $("#submitBtn").removeClass("d-none");
                $("#loader").addClass("d-none");
                /*$('#conf_crear_cuota_extraordinaria').trigger("reset");*/
            }
        });
    }

    $(".borrarUsuInm").click(function(event){
        event.preventDefault();
        var url = $(this).attr('href');
        $.bsAlert.confirm("¿Está seguro que desea eliminar este usuario?", function(){
            window.location = url;
        });
    });

    //eliminación de multa
    $(".borrarMulta").click(function(event) {
        event.preventDefault();
        var presupuestoId = $(this).attr("id");
        var split = presupuestoId.split("-");
        var id = split[1];
        $.bsAlert.confirm(
            "¿Está seguro que desea eliminar esta multa?",
            function() {
                eliminarMulta(base_url, id)
            }
        );
    });

    function eliminarMulta(url, id) {
        $.ajax({
            url: url + "multas/eliminar",
            type: 'POST',
            data: {
                id: id
            },
            beforeSend: function() {
                $("#pre-" + id).addClass("d-none");
                $("#delete-" + id).removeClass("d-none");
            },
            success: function(response) {
                console.log(response);

            },
            complete: function() {
                alertify.success("Multa eliminada");
                $("#pre-" + id).removeClass("d-none");
				$("#delete-" + id).addClass("d-none");
				$('#row-'+id).remove();
            }
        });
    }

    //Consulta los valores a pagar por recibo
    $('#input-recibo').on('change',function() {
        var recibo = []
        recibo = $('.selectpicker').val()   
        $.ajax({
            url: base_url + "abonos/traer_items_factura/"+recibo,
            type: 'GET',
            beforeSend: function() {
                $('#contentLoader').addClass('d-flex');
                $('#contentLoader').removeClass('d-none');
                $("#result").empty();
            },
            success: function(response) {
                $("#result").append(response).hide().fadeIn(500);
                console.log(response);
            },
            complete: function() {
                $('#contentLoader').removeClass('d-flex');
                $('#contentLoader').addClass('d-none');
            }
        });
      });

    //Consulta abonos hechos a un recibo
    $('#input-recibo-id').on('change',function() {
        var recibo = []
        recibo = $('.selectpicker').val()   
        $.ajax({
            url: base_url + "abonos/traer_detalle_abono_por_recibo/"+recibo,
            type: 'GET',
            beforeSend: function() {
                $('#contentLoader').addClass('d-flex');
                $('#contentLoader').removeClass('d-none');
                $("#result").empty();
            },
            success: function(response) {
                $("#result").append(response).hide().fadeIn(500);
                console.log(response);
            },
            complete: function() {
                $('#contentLoader').removeClass('d-flex');
                $('#contentLoader').addClass('d-none');
            }
        });
      });

    $("#submitBtn").click(function(){
            $.bsAlert.confirm("¿Está seguro que desea crear este presupuesto?. Esta acción puede tomar algunos minutos", function(){
                $("#submitBtn").addClass("d-none");
                $("#loader").removeClass("d-none");
                $( "#crear_presupuesto" ).submit();
        });
    });

    $("#submitAbono").click(function(){
        if ($('#input-total-pagar').val()==null) {
            alertify.error("Debe seleccionar una factura primero");
        }else{
            $.bsAlert.confirm("¿Está seguro/a que desea realizar este abono?", function(){
                $("#submitAbono").addClass("d-none");
                $("#loaderAbono").removeClass("d-none");
                $( "#crear_abono" ).submit();
            });
        }
    });

    $("#submitMulta").click(function(){
        $.bsAlert.confirm("¿Está seguro que desea crear esta multa?", function(){
            $("#submitMulta").addClass("d-none");
            $("#loaderMulta").removeClass("d-none");
            $( "#crear_multa" ).submit();
        });
    });

    $("#submitCuota").click(function(){
        $.bsAlert.confirm("¿Está seguro que desea crear esta multa?", function(){
            $("#submitCuota").addClass("d-none");
            $("#loaderCuota").removeClass("d-none");
            $("#crear_cuota_extraordinaria").submit();
        });
    });

   //eliminación de registro de abono
   $(document).on("click", '.borrarAbono',function(event) {
        event.preventDefault();
        var presupuestoId = $(this).attr("id");
        var split = presupuestoId.split("-");
        var id = split[1];
        $.bsAlert.confirm(
            "¿Está seguro que desea eliminar este registro?",
            function() {
                eliminarAbono(base_url, id)
            }
        );
    });

    function eliminarAbono(url, id) {
        $.ajax({
            url: url + "abonos/eliminar",
            type: 'POST',
            data: {
                abo_id: id
            },
            beforeSend: function() {
                $("#pre-" + id).addClass("d-none");
                $("#delete-" + id).removeClass("d-none");
            },
            success: function(response) {
                console.log(response);

            },
            complete: function() {
                alertify.success("Multa eliminada");
                $("#pre-" + id).removeClass("d-none");
                $("#delete-" + id).addClass("d-none");
                $('#row-'+id).remove();
            }
        });
    }
 
});