<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row ">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('recibos'); ?>">Inicio recibos de pago</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('Abonos'); ?>">Inicio abonos</a>
                                </li>
                                <li class="breadcrumb-item active">Consulta de abonos</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Abonos</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            
            <div class="row d-flex content-justified-center">
                <div class="form-group col-lg-12">
                    <label for="input-inmueble">Seleccione recibo*</label>
                    <div>
                        <select class="selectpicker col-lg-12" id="input-recibo-id" data-live-search="true" title="Seleccione una factura">
                            <?php foreach ($recibos as $recibo) { ?>
                                <option data-tokens="<?php echo $recibo->Rec_Pag_Numero; ?>"><?php echo $recibo->Rec_Pag_Numero; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>              
            </div>
            <div class="d-none justify-content-center" id="contentLoader">
                <div class="loader"></div>
            </div>
            <div id="result" class="d-flex content-justified-center">
                
            </div>  
        </div><!-- Fin container -->
    </div>
    <script>
        var base_url = "<?php echo base_url(); ?>"
    </script>
    <!-- Fin wrapper -->
    <?php $this->load->view('footer'); ?>
    <?php 
    if ($this->uri->segment(3)== "success")
    {?>
        <script>
            $(document).ready(function () {
                alertify.success("Cuota creada correctamente");
            });
        </script>
    <?php } ?>