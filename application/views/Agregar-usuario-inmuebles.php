<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- container -->
    <div class="container-fluid">

        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="pages-inicio-inmuebles.html">Inmuebles</a>
                            </li>
                        </ol>
                    </div>
                    <h4 class="page-title">Agregar usuario</h4>
                </div>
            </div>
        </div>
        <div class="card m-b-30">
            <div class="card-header">
                <label>
                    <h5>Agregar</h5>
                </label>
            </div>
            <div class="card-body">
            <?php echo form_open('inmuebles/agregar_usu');?>

                    <!-- Filtro Busqueda -->
                    <form>
                        <div class="form-row">
                            <div class="col-md-4 mb-4">
                            <label>Usuario</label>
                    <input type="hidden" value="<?php echo $inmueble->Inm_Id;?>" name="Inm_Id" required>

                    <select class="selectpicker col-12 d-block" data-live-search="true" name="Usu_Id" required>
                        <option value="">Seleccione</option>
                        <?php foreach($usuarios as $usuario): ?>
                        <option data-tokens="<?php echo $usuario->Usu_Nombres." ".$usuario->Usu_Apellidos;?>" value="<?php echo $usuario->Usu_Id;?>">
                            <?php echo $usuario->Usu_Nombres." ".$usuario->Usu_Apellidos;?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                    <datalist id="browsers" required>
                        <option value="">Seleccione</option>
                        <?php foreach($usuarios as $usuario): ?>
                        <?php echo $usuario->Usu_Nombres." ".$usuario->Usu_Apellidos;?>
                        </option>
                        <?php endforeach; ?>
                    </datalist>
                </div>
                            &nbsp;
                            &nbsp;
                            <div class="col-md-4 mb-4">
                            <label>Rol </label>
                                <select class="form-control" name="InmXUsu_Rol">
                                    <option value="Arrendatario">Arrendatario</option>
                                    <option value="Inquilino">Inquilino</option>
                                    <option value="Comodatario">Comodatario</option>
                                    <option value="Albacea">Albacea</option>
                                </select>
                                </div>             
                        </div>
                        <button type="submit" class="btn btn-primary" onclick="form.submit();this.disabled=true">Agregar</button>
                        <?php echo form_close();?>
                    </form>
                </form>
            </div>
        </div>

<!-- Fin titulo pagina y miga de pan -->
<?php $this->load->view('footer'); ?>