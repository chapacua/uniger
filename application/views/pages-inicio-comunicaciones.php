<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="#">Inicio</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Comunicaciones</a>
                                </li>
                            </ol>
                        </div>
                        <h4 class="page-title">Comunicaciones</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            <div class="row">
                <!-- Publicaciones -->
                <div class="col-lg-4 center-align">
                <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                        <a href="<? echo base_url('/Publicaciones/listar');?>">                                <div class="card-icon">
                                    <i class="fa fa-bullhorn"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Publicaciones</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- Eventos -->
                <div class="col-lg-4 center-align">
                <div class="card m-b-30 text-white bg-gray">
                    <div class="card-body">
                    <a href="<? echo base_url('/Mensajes/listar_Recibidos');?>"> 
                            <div class="card-icon">
                                <i class="fa fa-comments-o"></i>
                            </div>
                            <div class="card-text">
                                <p class="card-text">Mensajes</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
                <!-- Reportes Comunicaciones -->
                <div class="col-lg-4 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                        <a href="<? echo base_url('/Publicaciones/Reportes');?>"> 
                                <div class="card-icon">
                                    <i class="mdi mdi-chart-bar"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Reportes Comunicaciones</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="offset-lg-4 col-lg-4 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                        <a href="<? echo base_url('/Publicaciones/Peticiones');?>"> 
                                <div class="card-icon">
                                <i class="mdi mdi-file"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Peticion de publicaciones</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mensajes -->
        
    <!-- Fin container -->
    </div>
    <!-- Fin wrapper -->
    <?php $this->load->view('footer'); ?>