<?php $this->load->view('header'); ?>
    <!-- Fin MENU -->

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="inicio">Inicio</a>
                                </li>
                                <li class="breadcrumb-item active">Recibos de Pago</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Recibos de Pago</h4>   
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->


            <div class="row">
                <!-- Item -->
                <?php $i = 7; ?>
                <?php if ($this->session->userdata('Usu_Rol') == 0  || $duenos == TRUE) { ?>
                <div class="col-lg-3 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                            <a href="<? echo base_url('recibos/consultar');?>" data-step="<?php echo $i++ ?>" data-intro="Aquí el usuario puede consultar los recibos de pago generados." data-position='bottom'>
                                <div class="card-icon">
                                    <i class="mdi mdi-magnify"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Consultar recibos</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php }  ?>
                <?php if ($this->session->userdata('Usu_Rol') == 0) { ?>
                <!-- Item -->
                <div class="col-lg-3 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                            <a href="<? echo base_url('Abonos');?>" data-step="<?php echo $i++ ?>" data-intro="En abonos se puede gestionar los abonos hechos por los usuarios, así como obtener reportes y consultar los diferentes registros" data-position='right'>
                                <div class="card-icon">
                                    <i class="mdi mdi-format-annotation-plus"></i>
                                </div>
                                <div class="card-text">

                                    <p class="card-text">Abonos</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php }  ?>
                <?php if ($this->session->userdata('Usu_Rol') == 0 ) { ?>
                <!-- Item -->
                <div class="col-lg-3 ">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                            <a href="<?php echo base_url('Multas'); ?>"  data-step="<?php echo $i++ ?>" data-intro="En multas, el usuario puede llevar a cabo la gestión del proceso de penas pecuniarias que incluye la creación, eliminación y consulta de estos registros" data-position='right'>
                                <div class="card-icon">
                                    <i class="mdi mdi-cards"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Multas</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php }  ?>
                <?php if ($this->session->userdata('Usu_Rol') == 0) { ?>
                <!-- Item -->
                <div class="col-lg-3 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                            <a href="<?php echo base_url('Presupuestos'); ?>"   data-step="<?php echo $i++ ?>" data-intro="Los presupuestos del conjunto residencial pueden ser gestionados en esta sección, donde el administrador podrá consultar, crear y modificar presupuestos para un año determinado" data-position='left'>
                                <div class="card-icon">
                                    <i class="mdi mdi-book"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Presupuestos</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php }  ?>
                <?php if ($this->session->userdata('Usu_Rol') == 0) { ?>
                <!-- Item -->
                <div class="col-lg-3 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                            <a href="<?php echo base_url('Cuotas_Extraordinarias'); ?>"  data-intro="Aquí el administrador podrá crear cuotas extraordinarias, las cuales serán gestionadas hacia cada uno de los inmuebles y sus dueños que se encuentran registrados en la plataforma" data-position='right'>
                                <div class="card-icon">
                                    <i class="mdi mdi-cash-usd "></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Cuotas Extraordinarias</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php }  ?>        
        </div>
        <!-- Fin container -->

    </div>
    <!-- Fin wrapper -->

    <!-- Footer -->
    <?php $this->load->view('footer'); ?>