<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- container -->
    <div class="container-fluid">
        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item active">Registro</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Proveedores</h4>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <br>
                    <div class="card m-b-30">
                        <div class="card-header">
                                <h3>Registro de proveedor</h3>
                                <a>Los campos marcados con * son olbligatorios</a>
                        </div>
                        <div class="card-body">
                        <?php echo form_open('Proveedores/crear_proveedor');?>
                            <div class="form-group">
                                <label for="tipozona">
                                    <b>Tipo de Proveedor *</label>
                                <select class="form-control" id="tipoproveedor" name="Pro_Tipo">
                                    <option>Mantenimiento</option>
                                    <option>Prevención</option>
                                    <option>Instalación</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="NombreZona">Nombre del Proveedor *</label>
                                <?php echo form_error('Pro_Nombre'); ?>
                                <input type="text" placeholder="" class="form-control" name="Pro_Nombre" value= "<?php  echo  set_value ( 'Pro_Nombre' );  ?>">
                            </div>
                            <div class="form-group">
                                <label for="PeriodoMantenimiento">Teléfono del Proveedor *</label>
                                <?php echo form_error('Pro_Telefono'); ?>
                                <input type="text" placeholder="" class="form-control" name="Pro_Telefono" value= "<?php  echo  set_value ( 'Pro_Telefono' );  ?>">
                            </div>
                            <div class="form-group">
                                <label for="Estado">Dirección del proveedor *</label>
                                <?php echo form_error('Pro_Direccion'); ?>
                                <input type="text" placeholder="" class="form-control" name="Pro_Direccion" value= "<?php  echo  set_value ( 'Pro_Direccion' );  ?>">
                            </div>
                            <div class="form-group">
                                <label for="Descripcion">E-mail del Proveedor *</label>
                                <?php echo form_error('Pro_Correo'); ?>
                                <input type="mail" placeholder="" class="form-control" name="Pro_Correo" value= "<?php  echo  set_value ( 'Pro_Correo' );  ?>">
                            </div>
                            <div class="form-group">
                                <label for="Descripcion">Contacto del Proveedor *</label>
                                <?php echo form_error('Pro_Contacto'); ?>
                                <input type="text" placeholder="" class="form-control" name="Pro_Contacto" value= "<?php  echo  set_value ( 'Pro_Contacto' );  ?>">
                            </div>
                            <div class="form-group">
                                <label for="Descripcion">Descripción *</label>
                                <?php echo form_error('Pro_Descripcion'); ?>
                                <textarea class="form-control" id="Descripcion" rows="3" name="Pro_Descripcion" value= "<?php  echo  set_value ( 'Pro_Descripcion' );  ?>"></textarea>
                            </div>
                            <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary" onclick="form.submit();this.disabled=true">Registrar</button>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <?php $this->load->view('footer'); ?>