<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class perfil extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            //Validación de sesión
            if(!$this->session->userdata('logged_in'))
            {
                redirect('/');//Si no hay variable de sesión activa
            }
            else
            {
                $this->load->model('usuarios_Model');//Se instancia el modelo para muebles
            }
    }
    public function index()
    {
        $id = $this->session->userdata('Usu_Id');
        $data['datos'] = $this->usuarios_Model->obtener_Info_Usuario($id);
        $this->load->view('pages-perfil',$data);
    }
    
    public function actualizarContrasena()
    {
        $this->form_validation->set_error_delimiters("<div style='color: red;'>", "</div>");
        $this->form_validation->set_rules('password','clave actual', 'required|callback_password_check');
        $this->form_validation->set_rules('newPassword','clave nueva', 'required|min_length[6]');
        $this->form_validation->set_rules('newPasswordRepeat','repetir clave', 'required|matches[newPassword]');

        
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $password = sha1(md5($this->input->post('newPassword')));
            $this->usuarios_Model->actualizarContrasena($password);
            $this->session->set_flashdata('success', 'Contraseña actualizada con éxito');
            redirect('/perfil');
        }
        
    }

    public function actualizarInfoUsuario()
    {

        $this->form_validation->set_error_delimiters("<div style='color: red;'>", "</div>");
        $this->form_validation->set_rules('telefonoCel','telefono', 'required|numeric');
        
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {

        $data = array
        (
            'Usu_Telefono_Celular' => $this->input->post('telefonoCel'),
            'Usu_Telefono_Fijo' => $this->input->post('telefonoFijo')
        );

		//Seteo configuraciones para carga de imagen
		$config['upload_path']          = $_SERVER['DOCUMENT_ROOT'] ."/uniger/application/views/imgPerfil/"; //Eliminar "misrecetas" si es un servidor en producción
		$config['file_name']         	= 'perfil'.$this->session->userdata('Usu_Id');
		$config['allowed_types']        = 'gif|jpg|png';
		$config['overwrite']            = true;
		$config['max_size']             = 10240;
		
		$this->upload->initialize($config);//inicializo las configuraciones 
            if ( ! $this->upload->do_upload('foto_Perfil'))//Si cargar la imagen da error
            {
                    $error = array('error' => $this->upload->display_errors());
                    var_dump($error);
            }
            else
            {
                    $datosUpload = array('upload_data' => $this->upload->data());				
                    $data['Usu_Img_Perfil'] = $datosUpload['upload_data']['file_name'];//Agrego al array que contiene los datos del formulario el nombre del archivo
            }

            $this->usuarios_Model->actualizarInfoUsuario($data);
            $this->session->set_flashdata('success', 'Información actualizada con éxito');
            redirect('/perfil');
        }
        
    }

    public function password_check($password)
    {
        $result = $this->usuarios_Model->password_check(sha1(md5($password)));
        if($result)
        {
            return true;
        }
        else
        {
            $this->form_validation->set_message('password_check', 'La contraseña actual es incorrecta');
            return false;
        }
    }
}


?>