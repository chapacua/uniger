<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mantenimiento extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			//Validación de sesión
			if(!$this->session->userdata('logged_in'))
			{
				redirect('/');//Si no hay variable de sesión activa
			}
			else
			{
				$this->load->model('Mantenimiento_Model');//Se instancia el modelo
			}
	}
	public function index()
	{
		$this->load->view('pages-mantenimiento');
	}
	public function listar()
	{
		$datos['mantenimiento']=$this->Mantenimiento_Model->listar_mantenimiento();
		$this->load->view('pages-consultar-mantenimiento', $datos);

	

	}
	public function crear()
	{
		$datos['mantenimiento'] = $this->Mantenimiento_Model->listar_mantenimiento();
		$datos['listadoz']=$this->Mantenimiento_Model->listar_zonas();
		$datos['listadop']=$this->Mantenimiento_Model->listar_proveedor();
		$this->load->view('pages-registrar-mantenimiento', $datos);
		
	}
	public function crear_mantenimiento()
	{
		//setear validaciones
		$this->form_validation->set_error_delimiters("<div style='color: red;'>", "</div>");
		$this->form_validation->set_rules('Man_Tipo', 'Tipo de Mantenimiento', 'required');
		// $this->form_validation->set_rules('Man_Fecha', 'Fecha del Mantenimiento', 'required');
		$this->form_validation->set_rules('Man_Fecha', 'Fecha del Mantenimiento', 'required|callback_FechaMayor');
		$this->form_validation->set_rules('Man_Tiempo', 'Tiempo de Mantenimiento', 'required|numeric');
		$this->form_validation->set_rules('Man_Comentario_Inicial', 'Comentario inicial', 'required');
		$this->form_validation->set_rules('zona_comun', 'Zona común', 'required');
		$this->form_validation->set_rules('proveedor', 'Proveedor', 'required');

		//hacer validacion
		if ($this->form_validation->run() == FALSE)
		{
				$this->load->view('pages-registrar-mantenimiento');
		}

		else 
		{
			//Seteo las variables
			$datosMantenimiento = array
			(
				'Man_Tipo' => $this->input->post('Man_Tipo') ,
				'Man_Fecha' => $this->input->post('Man_Fecha'),
				'Man_Tiempo' => $this->input->post('Man_Tiempo'),
				'Zon_Com_Id' => $this->input->post('zona_comun'),
				'Man_Comentario_Inicial' => $this->input->post('Man_Comentario_Inicial'),
				
            );

            $result = $this->Mantenimiento_Model->crear_mantenimiento($datosMantenimiento);

            if ($result):
                 redirect('/Mantenimiento/listar/success');
            endif;
        }
    }

	public function borrar_mantenimiento(){
		$Token = $this->input->post('Res_Token');
		$result = $this->Mantenimiento_Model->borrar_mantenimiento($Token);

		if ($result):
			echo $result;
		endif;
        
	}
	public function editar_mantenimiento($id){
		$datos['informacion'] = $this->Mantenimiento_Model->listar_mantenimientoxId($id);
		$datos['mantenimiento'] = $this->Mantenimiento_Model->listar_mantenimiento();
		$datos['id']= $id;

		if($datos['informacion']){
			$this->load->view('pages-actualizar-mantenimiento', $datos); //crear la vista
		}else{
			echo "No se encontraron resultados";
		}
	}

	public function actualizar_mantenimiento(){

		$mantenimiento_id = $this->input->post('mantenimiento_id');

		$datosMantenimiento = array
		(
			'Man_Comentario_Final' => $this->input->post('Man_Comentario_Final'),
			'Man_Calificacion' => $this->input->post('Man_Calificacion'),
		);

		$this->Mantenimiento_Model->actualizar_mantenimiento($datosMantenimiento, $mantenimiento_id);

		redirect('/Mantenimiento/listar/update');
	}

	/**
     *Valida si la fecha seleccionada es de dias anteriores
     *
     * @param       date  $date
     * @return      true/false
    */
    public function FechaMayor($date) 
    {
		date_default_timezone_set('America/Bogota');
		
		$FechaHoy = date("Y-m-d");
		if ( $date != ''){
			if ($date < $FechaHoy)
			{
				$this->form_validation->set_message('FechaMayor', 'La fecha seleccionada no puede ser anterior a la fecha actual.');
				return false;
			} 
			else 
			{
				return true;
			}
		}
        
    }

}
