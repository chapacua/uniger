<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inmuebles extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			//Validación de sesión
			if(!$this->session->userdata('logged_in'))
			{
				redirect('/');//Si no hay variable de sesión activa
			}
			else
			{
				$this->load->model('Inmuebles_Model');//Se instancia el modelo para muebles
			}
	}
	public function index()
	{
		$this->load->view('pages-inicio-inmuebles');
	}
	public function listar()
	{
		$datos['inmuebles']=$this->Inmuebles_Model->listar_inmuebles();
		$this->load->view('pages-consultar-inmueble', $datos);
	}
	public function crear()
	{
		$datos['usuarios'] = $this->Inmuebles_Model->listar_usuarios();
		$this->load->view('pages-crear-inmuebles', $datos);
	}
	public function crear_inmueble()
	{
		//setear validaciones
		$this->form_validation->set_error_delimiters("<div style='color: red;'>", "</div>");
		$this->form_validation->set_rules('Inm_Tipo', 'Tipo de inmueble', 'required');
		$this->form_validation->set_rules('Usu_Id', 'Propietario', 'required');
		$this->form_validation->set_rules('Inm_Numero_Matricula', 'Número de matrícula','trim|required|is_unique[Inmuebles.Inm_Numero_Matricula]');
		$this->form_validation->set_rules('Inm_Id', 'Numero de apartamento', 'trim|required|is_unique[Inmuebles.Inm_Id]');
		$this->form_validation->set_rules('Inm_Area_Construida', 'Area construida', 'required|numeric');
		$this->form_validation->set_rules('Inm_Area_Privada', 'Area privada', 'required|numeric');
		$this->form_validation->set_rules('Inm_Coeficiente_Propiedad', 'Coeficiente de propiedad', 'required|decimal');

		//hacer validacion
		if ($this->form_validation->run() == FALSE)
		{
			$datos['usuarios'] = $this->Inmuebles_Model->listar_usuarios();
				$this->load->view('pages-crear-inmuebles', $datos);
		}

		else 
		{
			//Seteo las variables
			$datosInmueble = array
			(
				'Inm_Id' => $this->input->post('Inm_Id') ,
				'Inm_Tipo' => $this->input->post('Inm_Tipo') ,
				'Inm_Direccion' => "Torres de la giralda",
				'Inm_Numero_Matricula' => $this->input->post('Inm_Numero_Matricula'),
				'Inm_Area_Construida' => $this->input->post('Inm_Area_Construida'),
				'Inm_Area_Privada' => $this->input->post('Inm_Area_Privada'),
				'Inm_Coeficiente_Propiedad' => $this->input->post('Inm_Coeficiente_Propiedad')
			);


			$datosDueno = array
			(
				'Usu_Id' => $this->input->post('Usu_Id')
			);

			$result = $this->Inmuebles_Model->crear_inmueble($datosInmueble, $this->input->post('Usu_Id'));
		
			if($result):
				redirect('/Inmuebles/listar/success');
			endif;
		}

	}
	public function borrar_inmueble(){
	
		$Token = $this->input->post('Res_Token');
		$result = $this->Inmuebles_Model->borrar_inmueble($Token);

		if ($result):
			echo $result;
		endif;
	}
	public function editar_inmueble($id){
		$datos['informacion'] = $this->Inmuebles_Model->listar_inmueblesxId($id);
		$datos['usuarios'] = $this->Inmuebles_Model->listar_usuarios();
		$datos['id']= $id;
		if($datos['informacion']){
			$this->load->view('pages-actualizar-inmuebles', $datos);
		}else{
			echo "No se encontraron resultados";
		}
	}

	public function actualizar_inmueble(){

		$inmueble_id = $this->input->post('inmueble_id');

		$this->form_validation->set_error_delimiters("<div style='color: red;'>", "</div>");
		$this->form_validation->set_rules('Inm_Numero_Matricula', 'Numero matricula', 'required|numeric');
		$this->form_validation->set_rules('Inm_Area_Construida', 'Area construida', 'required|numeric');
		$this->form_validation->set_rules('Inm_Area_Privada', 'Area privada', 'required|numeric');
		$this->form_validation->set_rules('Inm_Coeficiente_Propiedad', 'Coeficiente de propiedad', 'required|decimal');

		if ($this->form_validation->run() == FALSE)
        {
            if (empty($this->input->post('inmueble_id'))) {
                redirect('inmuebles/listar');
            }else{
                $this->editar_inmueble($this->input->post('inmueble_id'));
            }
        }
        else
        {
		$datosInmueble = array
		(
			'Inm_Tipo' => $this->input->post('Inm_Tipo') ,
			'Inm_Direccion' => $this->input->post('Inm_Direccion'),
			'Inm_Numero_Matricula' => $this->input->post('Inm_Numero_Matricula'),
			'Inm_Area_Construida' => $this->input->post('Inm_Area_Construida'),
			'Inm_Area_Privada' => $this->input->post('Inm_Area_Privada'),
			'Inm_Coeficiente_Propiedad' => $this->input->post('Inm_Coeficiente_Propiedad')
		);

		$datosDueno = array
		(
			'Usu_Id' => $this->input->post('Usu_Id'),
		);
		
		$this->Inmuebles_Model->actualizar_inmueble($datosInmueble, $inmueble_id);
		$this->Inmuebles_Model->actualizar_usu_inmueble($datosDueno, $inmueble_id );

		redirect('/inmuebles/listar/update');
	}
}

	public function Buscar(){
		$this->form_validation->set_error_delimiters("<div style='color: red;'>", "</div>");
		$this->form_validation->set_rules('Inm_Id', 'Numero de apartamento', 'numeric');
		$this->form_validation->set_rules('Usu_Nombres', 'Nombre', 'alpha');
		$this->form_validation->set_rules('Usu_Apellidos', 'Apellido', 'alpha');
		$this->form_validation->set_rules('Inm_Area_Construida', 'Area construida','numeric');
		$this->form_validation->set_rules('Inm_Tipo', 'Tipo inmueble', 'alpha');

		if ($this->form_validation->run() == FALSE)
		{
				$this->load->view('pages-consultar-inmueble');
		}else{

		$BuscarDatos = array
		(
			'Inm_Id' => $this->input->post('Inm_Id'),
			'Usu_Nombres' => $this->input->post('Usu_Nombres'),
			'Usu_Apellidos'=> $this ->input->post('Usu_Apellidos'),
			'Inm_Area_Construida' => $this->input->post('Inm_Area_Construida'),
			'Inm_Tipo' => $this->input->post('Inm_Tipo')
		);

		$datos['inmuebles']=$this->Inmuebles_Model->Buscar($BuscarDatos);


		$this->load->view('pages-consultar-inmueble', $datos);

	}
}
	public function validarInmueblePorId($id)
	{
		$result = $this->Inmuebles_Model->validarInmueblePorId($id);
		print_r($result);
	}
	public function Reportes() {

		$datos['reportes']=$this->Inmuebles_Model->ReportesInmuebles();
		$this->load->view('pages-reportes-inmuebles', $datos);

	}
	 public function Pdf_inmueble($id) {
		$datos['inmuebles'] = $this->Inmuebles_Model->listar_inmueblesxId_pdf($id);
		$datos['usuarios'] = $this->Inmuebles_Model->listar_usuarios_pdf();
		$datos['id']= $id;
		if($datos['inmuebles']){
			$this->load->view('Pdf-inmuebles', $datos);
		}else{
			echo "No se encontraron resultados";
		}
	
	}
	public function editar_usu_inmueble($id){
		$datos['inmuebles']=$this->Inmuebles_Model->listar_usuarios_editar($id);
		$datos['usuarios'] = $this->Inmuebles_Model->listar_usuarios_Creados();
		$this->load->view('pages-editar-usu-inmueble', $datos);
	
		
	}
	public function agregar_usu(){
		
			$datosDueno = array
			(
				'Usu_Id' => $this->input->post('Usu_Id'),
				'Inm_Id' => $this->input->post('Inm_Id'),
				'InmXUsu_Rol' => $this->input->post('InmXUsu_Rol')

			);

			$result = $this->Inmuebles_Model->agregar_usu($datosDueno);
		
			if($result):
				redirect('/Inmuebles/editar_usu_inmueble/'.$this->input->post('Inm_Id').'/success');
			endif;
	}
	public function borrar_usuario($usu_id, $inm_id, $rol){
		if ($rol == "Due%C3%B1o") {
			$rol = "Dueño";
		}
		$this->Inmuebles_Model->borrar_usuario($usu_id, $inm_id, $rol);
		$this->Inmuebles_Model->listar_usuarios_editar($inm_id);	

		redirect('/Inmuebles/editar_usu_inmueble/'.$inm_id.'/delete');
		
		
	}
	
	public function Actulizar_usu(){
		$datosInmueble = array
		(
			'InmXUsu_Rol' => $this->input->post('InmXUsu_Rol'),
			'Usu_Id' => $this->input->post('Usu_Id'),
			'Inm_Id' => $this->input->post('Inm_Id')
		); 	

		$result = $this->Inmuebles_Model->Actulizar_Usu($datosInmueble);
	
		if($result):
			redirect('/Inmuebles/editar_usu_inmueble/'.$this->input->post('Inm_Id').'/update');
		endif;
		
		}

}
