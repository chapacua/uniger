<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gestion extends CI_Controller {

    public function __construct()
	{
			parent::__construct();
			//Validación de sesión

				$this->load->Model('Autenticacion_Model');//Se instancia el modelo para autenticación

	}
    
    /**
     * Recibe y valida credenciales de acceso, además setea variables de sesion o devuelve el usuario al inicio
     * @param N/A
     * @return N/A
     */
	public function index()
	{	    
        $email=$this->uri->segment(3); // 1stsegment
        $token=$this->uri->segment(4); // 2ndsegment 
        #consutlo que la cadena recibida sea la correcta    
	    $validar = $this->Autenticacion_Model->validarToken($email, $token);
	    #valido si es correcto
        if ($validar) 
        { 
	    	#envio a la vista para recuperar contraseña y envio los datos
	    	$variables['cadena_texto']=$email;
	    	$variables['correo']=$token;
	    	$this->load->view('pages-establecer-contrasena', $variables);
        } 
        else 
        {
            $this->session->set_flashdata('tokenVencido','true');
	    	#evnio al idnex
	    	redirect('/');
	    }
    }

    /**
     * Recibe y valida credenciales de acceso, además setea variables de sesion o devuelve el usuario al inicio
     * @param N/A
     * @return N/A
     */
	public function establecerContrasena()
	{	    
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

        $this->form_validation->set_rules('contra_uno', 'Contraseña', 'trim|required|min_length[5]|max_length[12]');
        $this->form_validation->set_rules('contra_dos', 'Confirmación de Contraseña', 'trim|required|min_length[5]|max_length[12]|matches[contra_uno]');
        
        
        if ($this->form_validation->run() == FALSE) {
            $email = $this->input->post('email');
            $token = $this->input->post('token');
            $this->session->set_flashdata('error', validation_errors());
            redirect('Gestion/index/'.$email.'/'.$token);
        } 
        else 
        {   
            $email = $this->input->post('email');
            $nueva_contrasena = $this->input->post('contra_uno');
            $this->Autenticacion_Model->establecerContrasena($nueva_contrasena);
            $this->Autenticacion_Model->deshabilitarToken($email);
            $this->session->set_flashdata('estableceContra', true);
            redirect('/');
        }
        


        

    }

}
