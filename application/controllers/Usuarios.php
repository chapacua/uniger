<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller 
{
    public function __construct()
	{
			parent::__construct();

                $this->load->Model('Usuarios_Model'); //Se instancia el modelo para autenticación
	}

    public function index()
    {
        $this->load->view('pages-inicio-usuarios');
    }  

    public function registro()
    {
        $this->load->view('pages-registro');
    }
    
    public function guia()
    {
        $this->load->view('pages-usuarios-guia');
    }

    public function acercaDe()
    {
        $this->load->view('pages-usuarios-acercaDe');
    }

    public function peticionesRegistro()
    {
        $data['usuarios'] = $this->Usuarios_Model->obtenerUsuariosNuevos();
        $data['historial'] = $this->Usuarios_Model->obtenerHistorialUsuarios();
        // var_dump($data);
        // die();
        $this->load->view('pages-usuarios-aceptacion', $data);
    }

    public function aprobar()
    {
        
        $incDatos = array(
            'Usu_Comentarios' => $this->input->post('comentariosAprobado'),
            'Usu_Id' => $this->encryption->decrypt($this->input->post('U_refAprobado')),
            'Usu_Rol' => $this->input->post('Rol_General')
        );
        $this->Usuarios_Model->aprobarUsuarios($incDatos['Usu_Id']);
        $this->Usuarios_Model->logRegistroUsuario($incDatos['Usu_Id'], "Aprobado");
       $this->session->set_flashdata('result', true);
        redirect('/usuarios/peticiones');
    }

    public function rechazar()
    {
        $incDatos = array(
            'Usu_Comentarios' => $this->input->post('comentariosRechazado'),
            'Usu_Id' => $this->encryption->decrypt($this->input->post('U_refRechazado')),
        );
        $this->Usuarios_Model->rechazarUsuario($incDatos['Usu_Id']);
        $this->Usuarios_Model->logRegistroUsuario($incDatos['Usu_Id'], "Rechazado", $incDatos['Usu_Comentarios']);
        $this->session->set_flashdata('result', true);
        redirect('/usuarios/peticiones');
    }

    public function detalleRegistroPorUsuario($email)
    {
        
        $datos = $this->Usuarios_Model->detalleRegistroPorUsuario($email);
        echo json_encode($datos);
    }

    public function listar()
    {
        $datos['Usuarios'] = $this->Usuarios_Model->listarUsuarios();
        //  var_dump($datos);
        // die();
        
        $this->load->view('pages-usuarios-listar', $datos);
    }

    public function detalle($id)
    {
        $datos['Usuario'] = $this->Usuarios_Model->detalleUsuarios($id);
       
        $this->load->model('Inmuebles_Model');
        $datos['Inmuebles'] = $this->Inmuebles_Model->inmueblesPorUsuario($id);
        $this->load->view('pages-usuarios-detalle', $datos);
        
    }

    public function actualizarUsuario()
    {           
        //Seteo las validaciones
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
        $this->form_validation->set_rules('nombres', 'Nombres', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('apellidos', 'Apellidos', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('tipo_documento', 'Tipo documento', 'required');
        $this->form_validation->set_rules('fecha_nacimiento', 'Fecha de nacimiento', 'required|callback_validarFecha');
        $this->form_validation->set_rules('tipo_perfil', 'Tipo de perfil', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('numero_documento', 'Número de documento', 'required|integer');
        $this->form_validation->set_rules('telefono', 'Teléfono', 'required|integer');
        $this->form_validation->set_rules('id_usuario', 'id_Usuario', 'required');
        $this->form_validation->set_rules('estado', 'Estado', 'required|alpha');

        //Hago la validación
        if ($this->form_validation->run() == FALSE)
        {
            if (empty($this->input->post('id_usuario'))) {
                redirect('usuarios/listar');
            }else{
                $this->detalle($this->input->post('id_usuario'));
            }
        }
        else
        {
            $datos = array 
            (
                'Usu_Nombres' => $this->input->post('nombres'),
                'Usu_Apellidos' => $this->input->post('apellidos'),
                'Usu_Tipo_Documento' => $this->input->post('tipo_documento'),
                'Usu_Fecha_Nacimiento' => $this->input->post('fecha_nacimiento'),
                'Usu_Rol' => $this->input->post('tipo_perfil'),
                'Usu_Numero_Documento' => $this->input->post('numero_documento'),
                'Usu_Telefono' => $this->input->post('telefono'),
                'Usu_Estado' => $this->input->post('estado')
            );
            $this->Usuarios_Model->actualizarUsuario($datos, $this->input->post('id_usuario'));
            
            $this->session->set_flashdata('success', true);
            $datos['Usuario'] = $this->Usuarios_Model->listarUsuarios($this->input->post('id_usuario'));
            $this->load->model('Inmuebles_Model');
            $datos['Inmuebles'] = $this->Inmuebles_Model->inmueblesPorUsuario($this->input->post('id_usuario'));
            $this->load->view('pages-usuarios-detalle', $datos);
        }
    }

    
    /**
     *Recibe los datos para completar el perfil de un usuario nuevo
     *
     * @param       date  $date
     * @return      true/false
     */
    public function completarInfo() 
    {
        $this->form_validation->set_rules('tipo_documento', 'Tipo de documento', 'required');
        $this->form_validation->set_rules('numero_documento', 'Número de documento', 'required|integer');
        $this->form_validation->set_rules('fecha_nacimiento', 'Número de nacimiento', 'required|date');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('incompleto', true);
            $this->session->set_flashdata('error', validation_errors());
            $this->session->set_flashdata('mensaje', set_value('numero_documento'));
            redirect('Inicio/');//Esto se debe cambiar por el inicio
        } else {
            $datos = array 
            (
                'Usu_Tipo_Documento' => $this->input->post('numero_documento'),
                'Usu_Numero_Documento' => $this->input->post('numero_documento'),
                'Usu_Fecha_Nacimiento' => $this->input->post('fecha_nacimiento')
            );
                
            $this->Usuarios_Model->completarInfo($datos);
            $this->session->set_flashdata('error', validation_errors());
            redirect('Inicio/');//Esto se debe cambiar por el inicio
        }
        
    }
    
    /**
     * Carga la vista para que el usuario ingrese su correo para enviar instrucciones para recuperar contraseña
     * @param N/A
     * @return N/A
     */
	public function recuperarContrasena()
	{	    
        $this->load->view('pages-recuperacontra');

    }
    
    /**
     *Carga los inmuebles asociados a un usuario para ser editados
     *
     * @param       date  $date
     * @return      true/false
     */
    public function inmueblesxusuario($id="") 
    {
        if($id== "")
        {
            redirect('Uni404');
        }
        else
        {
            $this->load->model('Inmuebles_Model');
            $data['inmuebles'] = $this->Inmuebles_Model->inmueblesPorUsuario($id);
            $this->load->view('pages-usuarios-inmueble-detalle', $data);
        }
    }

    
    /**
     *Carga los inmuebles asociados a un usuario para ser editados
     *
     * @param       date  $date
     * @return      true/false
     */
    public function editarDetallesInmuebles($inm_id="", $usu_id="") 
    {
        if($inm_id== "" || $usu_id == "")
        {
            redirect('Uni404');
        }
        else
        {
            $this->load->model('Inmuebles_Model');
            $data['inmuebles'] = $this->Inmuebles_Model->inmueblePorUsuario($inm_id, $usu_id);
            $this->load->view('pages-usuarios-inmueble-detalle-editar', $data);
        }
    }
    
    /**
     *Actualiza el rol de un usuario en un inmueble determinado
     *
     * @param       string rol
     * @return      true/false
     */
    public function actualizarInmueblesxUsuario() 
    {
        $this->form_validation->set_rules('rol', 'Rol', 'required');
        $this->form_validation->set_rules('Usu_Id', 'Inmueble', 'required');
        $this->form_validation->set_rules('Inm_Id', 'Usuario', 'required');

        $rol = $this->input->post('rol');
        $Usu_Id = $this->input->post('Usu_Id');
        $Inm_Id = $this->input->post('Inm_Id');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('success', 'Debe seleccionar algún rol');
            redirect(base_url("Usuarios/editarDetallesInmuebles/$Inm_Id/$Usu_Id"));
        }
        else
        {
            $this->load->model('Usuarios_Model');
            $this->Usuarios_Model->actualizarInmueblesxUsuario($rol, $Usu_Id, $Inm_Id);
            $this->session->set_flashdata('success', "Cambio realizado exitosamente");
            redirect(base_url("usuarios/detallesinmuebles/$Usu_Id"));//Esto se debe cambiar por el inicio


        }
    }


    /**
     *Valida si un campo tiene el formato de la fecha correctamente
     *
     * @param       date  $date
     * @return      true/false
    */
    public function reportes() 
    {
        $datos['datosRoles'] = $this->Usuarios_Model->rolesxUsuarios();
        $datos['datoTotalRoles'] = $this->Usuarios_Model->cantidadRolesxUsuarios();
        $datos['datoAprobaciones'] = $this->Usuarios_Model->reporteAprobaciones();
        $datos['datoTotalAprobaciones'] = $this->Usuarios_Model->totalUsuariosRegistrados();
        $this->load->view('pages-usuarios-reportes', $datos);
    }

    /**
     *Valida si un campo tiene el formato de la fecha correctamente
     *
     * @param       date  $date
     * @return      true/false
    */
    public function validarFecha($date) 
    {
        $pattern = '/^(19|20)\d\d[\-\/.](0[1-9]|1[012])[\-\/.](0[1-9]|[12][0-9]|3[01])$/';

        if (!preg_match($pattern, $date)) 
        {
            $this->form_validation->set_message('validarFecha', 'La fecha debe contener un formato correcto');
            return false;
        } 
        else 
        {
            return true;
        }
    }

    public function eliminar(){
		$Token = $this->input->post('Res_Token');
		$result = $this->Usuarios_Model->eliminarUsuarios($Token);

		if ($result):
			echo $result;
		endif;
    }

    public function historialUsuarios($id){
        $datos['historial'] = $this->Usuarios_Model->detalleRegistroPorUsuario($id);
            // var_dump($datos);
            // die();
        $this->load->view('pages-usuarios-historialDetalle',$datos);
    }
    public function detallesNuevo($id){
        $datos['historial'] = $this->Usuarios_Model->detalleUsuariosNuevos($id);
            // var_dump($datos);
            // die();
        $this->load->view('pages-usuarios-detallesNuevo',$datos);

    }
}

