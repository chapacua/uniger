<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuotas_Extraordinarias extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        //Validación de sesión
        if (!$this->session->userdata('logged_in')) {
            redirect('/'); //Si no hay variable de sesión activa
        } else {
            $this->load->model('Cuotas_Extraordinarias_Model'); //Se instancia el modelo para muebles
        }
    }

    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function index()
    {
        $this->load->view('pages-extraordinarias-inicio');
    }

    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function crear()
    {
        $datos['periodos'] = $this->Cuotas_Extraordinarias_Model->traer_periodos();
        $datos['meses'] = $this->Cuotas_Extraordinarias_Model->traer_meses_disponibles();
        $this->load->view('pages-extraordinarias-crear', $datos);
    }

    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function consultar()
    {
        $datos['cuotas_extraordinarias'] = $this->Cuotas_Extraordinarias_Model->traer_cuotas_extraordinarias();
        $datos['detalle_cuotas_extraordinarias'] = $this->Cuotas_Extraordinarias_Model->traer_detalles_cuotas_extraordinarias();
        $this->load->view('pages-extraordinarias-consultar', $datos);
    }

    /**
     *Crea cuota extraordinaria para un periodo en especifico
     *
     * @param       N/A
     * @return      json  $json
    */    
    public function guardar()
    {
        $this->form_validation->set_error_delimiters('<p class="mt-3 text-danger">', '</p>');
        $this->form_validation->set_rules('input-presupuesto', 'presupuesto', 'trim');
        $this->form_validation->set_rules('input-descripcion', 'descripcion', 'trim|required');
        $this->form_validation->set_rules('input-periodo', 'periodo', 'trim|required');
        $this->form_validation->set_rules('input-mes', 'mes', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {

            $this->crear();

        } else {

            $datos = array(
                'Cuo_Ext_Valor' => $this->remover_formato_numero($this->input->post('input-presupuesto')),
                'Cuo_Ext_Descripcion' => $this->input->post('input-descripcion'),
                'Per_Periodo' => $this->input->post('input-periodo'),
                'Mes_Id' => $this->input->post('input-mes')
            );
            $id_cuota = $this->Cuotas_Extraordinarias_Model->guardar_cuota_extraordinaria($datos);
            $this->Cuotas_Extraordinarias_Model->guardar_detalle_cuota_extraordinaria($datos['Cuo_Ext_Valor'], $id_cuota, $datos['Mes_Id'], $datos['Cuo_Ext_Descripcion']);

            redirect('Cuotas_Extraordinarias/consultar/success');
        }
    }
    

    /**
     *Valida si un campo tiene el formato de la fecha correctamente
     *
     * @param       date  $date
     * @return      true/false
    */
    function crear_recibos_pago($mes_actual, $inmuebleProcesar, $periodo_actual, $recibo_id){

        $this->db->select('d.Det_Cuo_Ext_Id');    
        $this->db->where('d.Mes_Id', $mes_actual);
        $this->db->where('d.Inm_Id', $inmuebleProcesar);
        $this->db->join('Cuotas_Extraordinarias c', 'd.Cuo_Ext_Id  = c.Cuo_Ext_Id');
        $this->db->join('Periodo p', 'p.Per_Periodo  = c.Per_Periodo');
        $this->db->where('p.Per_Periodo', $periodo_actual);
        $query = $this->db->get('Detalle_Cuotas_Extraordinarias d');

        foreach($query->result() as $row)
        {
            $datos = array(
                'Det_Cuo_Ext_Id' => $row->Det_Cuo_Ext_Id,
                'Det_Rec_Pag_Estado' => 'Por Imprimir',
                'Rec_Pag_Id' => $recibo_id
            );

            $this->db->insert('Detalle_Recibo_Pago', $datos);
            
        }
    }

    
    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function listar()
    {
        $datos['periodos_presupuesto'] = $this->Cuotas_Extraordinarias_Model->traer_periodos_sin_presupuesto();
        $datos['periodos'] = $this->Cuotas_Extraordinarias_Model->traer_periodos();
        $datos['meses'] = $this->Cuotas_Extraordinarias_Model->traer_meses_disponibles();
        $this->load->view('pages-facturacion-configuraciones', $datos);
    }

   
    /**
     *Valida si un campo contiene numeros
     *
     * @param       date  $date
     * @return      true/false
    */
    function valida_numero($text){
        $text = str_replace(".", "", $text);
        
        if (is_numeric(intval($text))) {
            return true;
        }else
        {
            $this->form_validation->set_message('valida_numero', 'Debe ingresar un valor numérico');
            return false;
        }
    }


    /**
     *Valida si un campo tiene el formato de la fecha correctamente
     *
     * @param       date  $date
     * @return      true/false
    */
    function remover_formato_numero($text){
        $text = str_replace(".", "", $text);
        return $text;
    }

    public function reportes()
    {
        $datos['promedios'] = $this->Cuotas_Extraordinarias_Model->reporter_promedio();

        $this->load->view('pages-cuotaextra-reportes', $datos);
    }
}