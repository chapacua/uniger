<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publicaciones extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			//Validación de sesión
			if(!$this->session->userdata('logged_in'))
			{
				redirect('/');//Si no hay variable de sesión activa
			}
			else
			{
				$this->load->model('Publicaciones_Model');//Se instancia el modelo para muebles
			}
	}
	public function index()
	{
		$this->load->view('pages-inicio-comunicaciones');
	}
	public function listar()
	{
		$datos['publicaciones']=$this->Publicaciones_Model->Listar_Publicaciones();
		$this->load->view('pages-publicaciones', $datos);
	}	
	public function crear()
	{
		$datos['publicaciones'] = $this->Publicaciones_Model->Listar_Publicaciones();
		$this->load->view('pages-agregar-publicacion', $datos);
	}
	public function Agregar_publicacion()
	{
		$this->form_validation->set_error_delimiters("<div style='color: red;'>", "</div>");
		$this->form_validation->set_rules('Pub_Contenido', 'Contenido', 'required');
		$this->form_validation->set_rules('Pub_Tipo_Informacion', 'Tipo de publicacion', 'required');

		//hacer validacion
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('pages-agregar-publicacion');
		}
		else 
		{
				//Seteo configuraciones para carga de imagen
				$config['upload_path']          = $_SERVER['DOCUMENT_ROOT'] ."/uniger/application/views/Publicaciones/"; //Eliminar "misrecetas" si es un servidor en producción
				$config['file_name']         	= 'perfil'.$this->session->userdata('Usu_Id');
				$config['allowed_types']        = 'gif|jpg|png';
				$config['overwrite']            = true;
				$config['max_size']             = 10240;
				
				$this->upload->initialize($config);//inicializo las configuraciones 
					if ( ! $this->upload->do_upload('Pub_Imagen'))//Si cargar la imagen da error
					{
							$error = array('error' => $this->upload->display_errors());
							var_dump($error);
					}
					else
					{
							$datosUpload = array('upload_data' => $this->upload->data());				
							$data['Pub_Imagen'] = $datosUpload['upload_data']['file_name'];//Agrego al array que contiene los datos del formulario el nombre del archivo
					}
			//Seteo las variables
			$datosPublicacion = array
			(
				'Pub_Contenido' => $this->input->post('Pub_Contenido'),
				'Pub_Tipo_Informacion' => $this->input->post('Pub_Tipo_Informacion'),
				'Pub_Imagen' => $this->input->post('Pub_Imagen'),
				'Usu_Id' => $this->session->userdata('Usu_Id'),
			);

			$result = $this->Publicaciones_Model->Agregar_Publicacion($datosPublicacion);

			if($result):
				redirect('/Publicaciones/listar/success');
			endif;
		}
	}
	public function Editar($id){
		$datos['publicacion'] = $this->Publicaciones_Model->listar_publicacionesXId($id);
		$datos['publicacion'] = $this->Publicaciones_Model->Listar_Publicaciones();
		$datos['id']= $id;
		if($datos['publicacion']){
			$this->load->view('pages-editar-publicaciones', $datos);
		}else{
			echo "No se encontraron resultados";
		}
	}
	public function actualizar_publicacion(){

		$publicacion_id = $this->input->post('publicacion_id');
		$datosPublicacion = array
		(
			'Pub_Contenido' => $this->input->post('Pub_Contenido'),
			'Usu_Id' => $this->session->userdata('Usu_Id'),
		);

		$this->Publicaciones_Model->Editar($datosInmueble, $publicacion_id);

		redirect('/Publicaciones/listar/update');
	}
	public function Borrar($id){
		$this->Publicaciones_Model->Borrar_Pub($id);
		redirect('/Publicaciones/listar/delete');
	}

	public function Reportes(){
		$datos['reportes']=$this->Publicaciones_Model->Reportes();
		$this->load->view('pages-reportes-comunicaciones', $datos);

	}
}