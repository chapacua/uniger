<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Multas extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        //Validación de sesión
        if (!$this->session->userdata('logged_in')) {
            redirect('/'); //Si no hay variable de sesión activa
        } else {
            $this->load->model('Multas_Model'); //Se instancia el modelo para muebles
        }
    }

    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function index()
    {
        $this->load->view('pages-multas-inicio');
    }

    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function crear()
    {
        $datos['usuarios'] =  $this->Multas_Model->traer_usuarios_registrados();
        $datos['inmuebles'] =  $this->Multas_Model->traer_inmuebles();
        $this->load->view('pages-multas-crear', $datos);
    }

    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function consultar()
    {
        $datos['multas'] = $this->Multas_Model->traer_multas();

        $this->load->view('pages-multas-consultar', $datos);
    }
    
    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function eliminar()
    {
        $multa_id = $this->input->post('id');
        $this->Multas_Model->eliminar_multa($multa_id);
        echo $multa_id;
    }

    /**
     *Crea cuota extraordinaria para un periodo en especifico
     *
     * @param       N/A
     * @return      json  $json
    */    
    public function guardar()
    {
        $this->form_validation->set_error_delimiters('<p class="mt-3 text-danger">', '</p>');
        $this->form_validation->set_rules('input-valor', 'valor', 'trim|required|min_length[1]|max_length[8]|callback_valida_numero');
        $this->form_validation->set_rules('input-fecha', 'fecha', 'trim|required|callback_valida_fecha_futuro');
        $this->form_validation->set_rules('input-inmueble', 'inmueble', 'trim|required');
        $this->form_validation->set_rules('input-usuario-registrado', '¿Se encuentra registrado el infractor en la plataforma? ', 'trim|required');
        $this->form_validation->set_rules('input-pagador', '¿Quien pagará la multa? ', 'trim|required');
        $this->form_validation->set_rules('input-usuario-id', 'usuario infractor', 'trim|required|callback_valida_registrado');
        $this->form_validation->set_rules('input-usuario', 'usuario infractor', 'trim|callback_valida_no_registrado');
        $this->form_validation->set_rules('input-descripcion', 'descripcion', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {

            $this->crear();

        } else {
            $usuario_registrado = $this->input->post('input-usuario-registrado');
            if($usuario_registrado == "Si")
            {
                $usu_id = $this->input->post('input-usuario-id');
                $usu_nombre = null;
            }else
            {
                $usu_id = null;
                $usu_nombre = $this->input->post('input-usuario');

            }
            $datos = array(
                'Mul_Valor' => $this->remover_formato_numero($this->input->post('input-valor')),
                'Mul_Fecha' => $this->input->post('input-fecha'),
                'Inm_Id' => $this->input->post('input-inmueble'),
                'Mes_Id' => date("m"),
                'Usu_Id' => $usu_id,
                'Usu_Nombre' => $usu_nombre,
                'Mul_Tipo' => $this->input->post('input-pagador'),
                'Per_Periodo' => date("Y"),
                'Mul_Descripcion' => $this->input->post('input-descripcion')
            );
            $this->Multas_Model->guardar_multa($datos);
            redirect('multas/consultar/success');
        }
    }

        
	/****************/
	/*              */
	/* Configuracion*/
	/*              */
    /****************/
    
    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function listar()
    {
        $datos['periodos_presupuesto'] = $this->Multas_Model->traer_periodos_sin_presupuesto();
        $datos['periodos'] = $this->Multas_Model->traer_periodos();
        $datos['meses'] = $this->Multas_Model->traer_meses_disponibles();
        $this->load->view('pages-facturacion-configuraciones', $datos);
    }
        
    /**
     *Crea un nuevo periodo y cambia el periodo activo actual
     *
     * @param       date  $date
     * @return      true/false
    */    
    public function actualizar_periodo()
    {
        $this->Multas_Model->nuevo_periodo();
    }
        
    /**
     *Crea presupuesto para un año, agregando el valor mensual a cada aptartamento
     *
     * @param       N/A
     * @return      json  $json
    */    
    public function crear_presupuesto()
    {
        $this->form_validation->set_rules('input-presupuesto', 'presupuesto', 'trim|required');
        $this->form_validation->set_rules('input-descripcion', 'descripcion', 'trim|required');
        $this->form_validation->set_rules('input-periodo', 'periodo', 'trim|required|is_unique[Presupuesto.Per_Periodo]');
        
        if ($this->form_validation->run() == FALSE) {
            $json = array(
                'presupuesto'   => form_error('input-presupuesto', '<p class="mt-3 text-danger">', '</p>'),
                'descripcion'   => form_error('input-descripcion', '<p class="mt-3 text-danger">', '</p>'),
                'periodo'       => form_error('input-periodo', '<p class="mt-3 text-danger">', '</p>'),
            );

            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
        } else {

            $datos = array(
                'Pre_Valor' => $this->remover_formato_numero($this->input->post('input-presupuesto')),
                'Pre_Descripcion' => $this->input->post('input-descripcion'),
                'Per_Periodo' => $this->input->post('input-periodo')
            );
            $id_presupuesto = $this->Multas_Model->nuevo_presupuesto($datos);
            $this->Multas_Model->nuevo_detalle_presupuesto($datos['Pre_Valor'], $id_presupuesto, $datos['Pre_Descripcion']);

        }
    }
        
    /**
     *Crea cuota extraordinaria para un periodo en especifico
     *
     * @param       N/A
     * @return      json  $json
    */
    public function crear_cuota_extraordinaria()
    {
        $this->form_validation->set_rules('input-presupuesto', 'presupuesto', 'trim|required');
        $this->form_validation->set_rules('input-descripcion', 'descripcion', 'trim|required');
        $this->form_validation->set_rules('input-periodo', 'periodo', 'trim|required');
        $this->form_validation->set_rules('input-mes', 'mes', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $json = array(
                'presupuesto'   => form_error('input-presupuesto', '<p class="mt-3 text-danger">', '</p>'),
                'descripcion'   => form_error('input-descripcion', '<p class="mt-3 text-danger">', '</p>'),
                'periodo'       => form_error('input-periodo', '<p class="mt-3 text-danger">', '</p>'),
                'mes'           => form_error('input-mes', '<p class="mt-3 text-danger">', '</p>'),
            );

            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
        } else {

            $datos = array(
                'Cuo_Ext_Valor' => $this->remover_formato_numero($this->input->post('input-presupuesto')),
                'Cuo_Ext_Descripcion' => $this->input->post('input-descripcion'),
                'Per_Periodo' => $this->input->post('input-periodo'),
                'Mes_Id' => $this->input->post('input-mes')
            );
            $id_presupuesto = $this->Multas_Model->nueva_cuota_extraordinaria($datos);
            $this->Multas_Model->nuevo_detalle_cuota_extraordinaria($datos['Pre_Valor'], $id_presupuesto, $datos['Pre_Descripcion']);
        }
    }
                
    /**
     *Valida si un campo contiene numeros
     *
     * @param       date  $date
     * @return      true/false
    */
    function valida_numero($text){
        $text = str_replace(".", "", $text);
        if (is_numeric(intval($text))) {
            return true;
        }else
        {
            $this->form_validation->set_message('valida_numero', 'Debe ingresar un valor numérico');
            return false;
        }
    }

    /**
     *Valida si un campo tiene el formato de la fecha correctamente
     *
     * @param       date  $date
     * @return      true/false
    */
    function remover_formato_numero($text){
        $text = str_replace(".", "", $text);
        return $text;
    }

        /**
     *Valida si un campo tiene el formato de la fecha correctamente
     *
     * @param       date  $date
     * @return      true/false
    */
    public function valida_registrado($int) 
    {
        $usuario_registrado = $this->input->post('input-usuario-registrado');

        if($usuario_registrado == "Si" AND empty($int))
        {
            $this->form_validation->set_message('valida_registrado', 'Debe seleccionar un usuario');
            return false;
        }else
        {
            return true;
        }

    }

        /**
     *Valida si un campo tiene el formato de la fecha correctamente
     *
     * @param       date  $date
     * @return      true/false
    */
    public function valida_no_registrado($int) 
    {
        $usuario_registrado = $this->input->post('input-usuario-registrado');

        if($usuario_registrado == "No" AND empty($int))
        {
            $this->form_validation->set_message('valida_no_registrado', 'Debe escribir el nombre de un usuario');
            return false;
        }else
        {
            return true;
        }

    }

    /**
     *Valida que un campo fecha no se haya ingresado una fecha mayor a la del dia de hoy
     *
     * @param       date  $date
     * @return      true/false
    */
    public function valida_fecha_futuro($fecha) 
    {
        $date_input = date($fecha); 

        $date_limit = date('Y-m-d');

        if (strtotime($date_input) > strtotime($date_limit)) {
            $this->form_validation->set_message('valida_fecha_futuro', 'La fecha no debe ser mayor al dia de hoy');
            return false;
        }
        else
        {
            return true;
        }

    }

    public function reportes_multas(){

        $datos['reportes'] = $this->Multas_Model->reportes_multas();
        $datos['inmuebles'] = $this->Multas_Model->multas_inmuebles();
        $datos['promedios'] = $this->Multas_Model->reportes_promedio();
        $this->load->view('pages-multas-reportes', $datos);
    }
    
}