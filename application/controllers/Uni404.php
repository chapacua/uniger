<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uni404 extends CI_Controller {

	public function __construct(){
		parent::__construct();
    }
    
    //Método para loguearse al sistema

    public function index()
    {
        $this->output->set_status_header('404');
        $this->load->view('pages-404.php');
    }
}
