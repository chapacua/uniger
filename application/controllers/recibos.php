<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recibos extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        //Validación de sesión
        if (!$this->session->userdata('logged_in')) {
            redirect('/'); //Si no hay variable de sesión activa
        } else {
            $this->load->model('Recibo_Model'); //Se instancia el modelo para muebles
        }
    }
    public function index()
    {
        $datos['duenos'] = $this->Recibo_Model->filtrar_dueno();
        // var_dump($datos);
        // die();
        $this->load->view('pages-Facturacion-inicio',$datos);
    }


    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function consultar()
    {

        $datos['recibos'] = $this->Recibo_Model->traer_datos_pdf();

        $this->load->view('pages-recibos-consultar', $datos);
    }

    /**
     *Este método crea los distintos registros en la tabla recibos para los recibos un mes 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function guardar_recibos()
    {
        $inmuebles= $this->Recibo_Model->traer_inmuebles();
        $periodo_actual = $this->Recibo_Model->traer_periodo_actual();
        $mes_actual = (date('m'));
        $mes_anterior = ($mes_actual == "1") ? "12" : $mes_actual -1 ; //Si el mes es enero, hace que el mes anterior sea diciembre
        $bandera = 0;

        foreach($inmuebles AS $inmueble){

            $inmuebleProcesar = $inmueble->Inm_Id;
            $nombre_pdf = date('Ymd')."-".$inmuebleProcesar.".pdf";
            $recibo_id = $this->Recibo_Model->crear_recibo($nombre_pdf);
            $this->load->model('Multas_Model');
            $this->Multas_Model->crear_recibos_pago($mes_anterior, $inmuebleProcesar, $periodo_actual, $recibo_id);
            $this->load->model('Cuotas_Extraordinarias_Model');
            $this->Cuotas_Extraordinarias_Model->crear_recibos_pago($mes_actual, $inmuebleProcesar, $periodo_actual, $recibo_id);
            $this->load->model('Presupuestos_Model');
            $this->Presupuestos_Model->crear_recibos_pago($mes_actual, $inmuebleProcesar, $periodo_actual, $recibo_id);

            $this->crear_pdf($mes_anterior, $inmuebleProcesar, $periodo_actual);
            $bandera++;
        }

        echo "<h3>Fueron generados $bandera recibos</h3>"; 
    }
    
    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */ 
    public function crear_pdf($mes_anterior, $inmuebleProcesar, $periodo_actual)
    {
        $datos['info'] = $this->Recibo_Model->traer_info_pdf($inmuebleProcesar);
        $datos['multas'] = $this->Recibo_Model->traer_multas($mes_anterior, $inmuebleProcesar, $periodo_actual);
        $datos['cuotas_extraordinarias'] = $this->Recibo_Model->traer_cuotas_extraordinarias($mes_anterior, $inmuebleProcesar, $periodo_actual);
        $datos['presupuestos'] = $this->Recibo_Model->traer_presupuestos($mes_anterior, $inmuebleProcesar, $periodo_actual);
        echo '<pre>';
        var_dump($datos);
        echo '</pre>';
        $this->load->library('invoicepdf');
        $invoice = new invoicepdf();
        ini_set('memory_limit', '-1');
        /* Header Settings */
        $invoice->setTimeZone('America/Bogota');
        $invoice->setLogo(base_url('application/views/assets/images/logo-dark.png'));
        $invoice->setColor("#3949AB");
        $invoice->setType("Recibo de pago");
        $invoice->setReference(date('Ymd')."-".$inmuebleProcesar);
        $invoice->setDate(date('Y - m - d', time()));
        $invoice->setTime(date('h:i:s A', time()));
        $invoice->setDue(date('Y - m - d', strtotime('+10 days')));
        $invoice->setFrom(array(
            "Antioquia",
            "Medellin" ,
            "Torres de la Giralda",
            "Carrera 39 # 47 – 53",
            ""
        ));
        $invoice->setTo(array(
            $datos['info']->nombres,
            $datos['info']->Inm_Tipo,
            "$inmuebleProcesar",
            "",
            ""
        ));
        /* Adding Items in table */
        $total = 0;
        /* Adding multas */
        if(!empty($datos['multas'])){
            foreach($datos['multas'] as $multa)
            {
                $invoice->addItem("Multa", $multa->Mul_Descripcion, 1,0,$multa->Mul_Valor,0,$multa->Mul_Valor);

                $total = $total + $multa->Mul_Valor;
            }
        }
        /* Adding cuotas extraordinarias */
        if(!empty($datos['cuotas_extraordinarias'])){
            foreach($datos['cuotas_extraordinarias'] as $cuota)
            {
                $invoice->addItem("Cuota Extraordinaria", $cuota->Det_Cuo_Ext_Descripcion, 1,0,$cuota->Det_Cuo_Ext_Valor,0,$cuota->Det_Cuo_Ext_Valor);

                $total = $total + $cuota->Det_Cuo_Ext_Valor;
            }
        }
        /* Adding administración */
        if(!empty($datos['presupuestos'])){
            foreach($datos['presupuestos'] as $presupuesto)
            {
                $invoice->addItem("Administración", $presupuesto->Det_Pre_Descripcion, 1,0,$presupuesto->Det_Pre_Valor,0,$presupuesto->Det_Pre_Valor);

                $total = $total + $presupuesto->Det_Pre_Valor;
            }
        }
        /* Add totals */

        $invoice->addTotal("Total Pagar", $total, true);
        /* Set badge */
        
        /* Add title */
        $invoice->addTitle("");
        /* Add Paragraph */
        // $invoice->addParagraph("No item will be replaced or refunded if you don't have the invoice with you. You can refund within 2 days of purchase.");
        /* Set footer note */
        $invoice->setFooternote("UNIGER");
        /* Render */
        $invoice->render( __DIR__."/recibos/".date('Ymd')."-".$inmuebleProcesar.".pdf", 'F');

        /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
          
    }


	/****************/
	/*              */
	/* Configuracion*/
	/*              */
    /****************/
    
    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function conf_listar()
    {
        $datos['periodos_presupuesto'] = $this->Recibo_Model->conf_traer_periodos_sin_presupuesto();
        $datos['periodos'] = $this->Recibo_Model->conf_traer_periodos();
        $datos['meses'] = $this->Recibo_Model->conf_traer_meses_disponibles();
        $this->load->view('pages-facturacion-configuraciones', $datos);
    }
        
    /**
     *Crea un nuevo periodo y cambia el periodo activo actual
     *
     * @param       date  $date
     * @return      true/false
    */    
    public function conf_actualizar_periodo()
    {
        $this->Recibo_Model->conf_nuevo_periodo();
    }
        

                
    /**
     *Valida si un campo tiene el formato de la fecha correctamente
     *
     * @param       date  $date
     * @return      true/false
    */
    function remover_formato_numero($text){
        $text = str_replace(".", "", $text);
        return $text;
    }

        /**
     *Crea un nuevo periodo y cambia el periodo activo actual
     *
     * @param       date  $date
     * @return      true/false
    */    
    public function actualizar_periodo()
    {
        $this->Cuotas_Extraordinarias_Model->nuevo_periodo();
    }
}