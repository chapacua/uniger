<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presupuestos extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        //Validación de sesión
        if (!$this->session->userdata('logged_in')) {
            redirect('/'); //Si no hay variable de sesión activa
        } else {
            $this->load->model('Presupuestos_Model'); //Se instancia el modelo para muebles
        }
    }
    
    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
     */ 
    public function index()
    {
        $this->load->view('pages-presupuestos-inicio');
    }
    
    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
     */ 
    public function crear()
    {
        $datos['periodos_presupuesto'] = $this->Presupuestos_Model->conf_traer_periodos_sin_presupuesto();
        $datos['periodos'] = $this->Presupuestos_Model->conf_traer_periodos();
        $datos['meses'] = $this->Presupuestos_Model->conf_traer_meses_disponibles();
        $this->load->view('pages-presupuestos-crear', $datos);
    }
    
    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
     */ 
    public function consultar()
    {
        $datos['presupuestos'] = $this->Presupuestos_Model->traer_presupuestos();
        $datos['detalle_presupuestos'] = $this->Presupuestos_Model->traer_detalles_pesupuestos();
        $this->load->view('pages-presupuestos-consultar', $datos);
    }
        
    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function eliminar()
    {
        $id = $this->input->post('id');
        $periodo = $this->Presupuestos_Model->borrar_presupuesto($id);
        $this->Presupuestos_Model->borrar_detalles_presupuesto($periodo);
        $this->load->model('cuotas_Extraordinarias_Model');
        $cuota_extraordinaria = $this->Cuotas_Extraordinarias_Model->borrar_cuota_extraordinaria($periodo);
        $this->Cuotas_Extraordinarias_Model->borrar_detalles_extraordinaria($cuota_extraordinaria);
        $this->load->model('multas_Model');
        $this->Multas_Model->borrar_multas($periodo);

        echo $periodo;
    }
        
    /**
     *Lista 
     *
     * @param       date  $date
     * @return      true/false
    */ 
    public function borrar_detalles_presupuesto($id)
    {
        $periodo = $this->Presupuestos_Model->borrar_detalles_presupuesto($id);
        return $periodo;
    }
        
    /**
     *Crea un nuevo periodo y cambia el periodo activo actual
     *
     * @param       date  $date
     * @return      true/false
    */    
    public function conf_actualizar_periodo()
    {
        $this->Presupuestos_Model->conf_nuevo_periodo();
    }
        
    /**
     *Crea presupuesto para un año, agregando el valor mensual a cada aptartamento
     *
     * @param       N/A
     * @return      json  $json
    */    
    public function guardar()
    {
        $this->form_validation->set_error_delimiters('<p class="mt-3 text-danger">', '</p>');
        $this->form_validation->set_rules('input-presupuesto', 'presupuesto', 'trim|required|min_length[1]|max_length[12]|callback_valida_numero');
        $this->form_validation->set_rules('input-descripcion', 'descripcion', 'trim|required');
        $this->form_validation->set_rules('input-periodo', 'periodo', 'trim|required|is_unique[Presupuestos.Per_Periodo]');
        
        if ($this->form_validation->run() == FALSE) {

            $this->crear();
            
        } else {

            $datos = array(
                'Pre_Valor' => $this->remover_formato_numero($this->input->post('input-presupuesto')),
                'Pre_Descripcion' => $this->input->post('input-descripcion'),
                'Per_Periodo' => $this->input->post('input-periodo')
            );
            $id_presupuesto = $this->Presupuestos_Model->conf_nuevo_presupuesto($datos);
            $this->Presupuestos_Model->conf_nuevo_detalle_presupuesto($datos['Pre_Valor'], $id_presupuesto, $datos['Pre_Descripcion']);

            redirect('presupuestos/consultar/success');
        }
    }
        
    /**
     *Crea cuota extraordinaria para un periodo en especifico
     *
     * @param       N/A
     * @return      json  $json
    */    
    public function conf_crear_cuota_extraordinaria()
    {
        $this->form_validation->set_rules('input-presupuesto', 'presupuesto', 'trim|required');
        $this->form_validation->set_rules('input-descripcion', 'descripcion', 'trim|required');
        $this->form_validation->set_rules('input-periodo', 'periodo', 'trim|required');
        $this->form_validation->set_rules('input-mes', 'mes', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $json = array(
                'presupuesto'   => form_error('input-presupuesto', '<p class="mt-3 text-danger">', '</p>'),
                'descripcion'   => form_error('input-descripcion', '<p class="mt-3 text-danger">', '</p>'),
                'periodo'       => form_error('input-periodo', '<p class="mt-3 text-danger">', '</p>'),
                'mes'           => form_error('input-mes', '<p class="mt-3 text-danger">', '</p>'),
            );

            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
        } else {

            $datos = array(
                'Cuo_Ext_Valor' => $this->remover_formato_numero($this->input->post('input-presupuesto')),
                'Cuo_Ext_Descripcion' => $this->input->post('input-descripcion'),
                'Per_Periodo' => $this->input->post('input-periodo'),
                'Mes_Id' => $this->input->post('input-mes')
            );
            $id_presupuesto = $this->Presupuestos_Model->conf_nueva_cuota_extraordinaria($datos);
            $this->Presupuestos_Model->conf_nuevo_detalle_cuota_extraordinaria($datos['Pre_Valor'], $id_presupuesto, $datos['Pre_Descripcion']);
        }
    }
                
    /**
     *Valida si un campo tiene el formato de la fecha correctamente
     *
     * @param       date  $date
     * @return      true/false
    */
    function remover_formato_numero($text){
        $text = str_replace(".", "", $text);
        return $text;
    }

    
        /**
     *Valida si un campo contiene numeros
     *
     * @param       date  $date
     * @return      true/false
    */
    public function reportes()
    {
        $datos['presupuestos'] = $this->Presupuestos_Model->reportes_anual();
        $datos['reportes'] = $this->Presupuestos_Model->reportes_inmueble();
        // var_dump($datos);
        // die();
        $this->load->view('pages-presupuesto-reportes',$datos);
    }

        /**
     *Valida si un campo contiene numeros
     *
     * @param       date  $date
     * @return      true/false
    */
    function valida_numero($text){
        $text = str_replace(".", "", $text);
        if (is_numeric(intval($text))) {
            return true;
        }else
        {
            $this->form_validation->set_message('valida_numero', 'Debe ingresar un valor numérico');
            return false;
        }
    }
}